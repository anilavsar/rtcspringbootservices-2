package com.dci.rest.auth.service;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.rest.auth.bdo.AuthBDO;
import com.dci.rest.model.APIModel;
import com.dci.rest.model.Response;
@Service
public class AuthService {
		
	@Autowired
	AuthBDO oauthBDO;

	public Response<Object> registerClient(HttpServletRequest request,APIModel aPIBean) {		
		return oauthBDO.registerClient(request,aPIBean);
	}
	public Response<Object> updateregisterRestUser(HttpServletRequest request,APIModel aPIBean) {		
		return oauthBDO.registerClient(request,aPIBean);
	}
	public Response<Object> getApiList(Map<String, String> pathVariablesMap,HttpServletRequest request) throws SQLException {		
		return oauthBDO.getApiList(pathVariablesMap,request);
	}
	public Response<Object> removeAPI(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return oauthBDO.removAPI(pathVariablesMap,request);
	}
	public APIModel getApiDetailsByURL(HttpServletRequest request,String baseUrl) throws SQLException {		
		return oauthBDO.getApiDetailsByURL(request,baseUrl);
	}
	public APIModel userAuthServerDetails(HttpServletRequest request,String accessKey) throws SQLException {		
		return oauthBDO.userAuthServerDetails(request,accessKey);
	}
/*	public Response<Object> getRequestToken(Map<String, String> pathVariablesMap, HttpServletRequest request, HttpServletResponse resp,APIModel apiModel) throws Exception {
		return oauthBDO.getRequestToken(pathVariablesMap,request,resp,apiModel);
	}
	*/
/*	public Response<Object> addRequestToken(APIBean aPIBean) throws SQLException {		
		return authBDO.addRequestToken(aPIBean);
	}
	public Response<Object> addAccessToken(APIBean aPIBean) throws SQLException {		
		return authBDO.addAccessToken(aPIBean);
	}*/
	
}
