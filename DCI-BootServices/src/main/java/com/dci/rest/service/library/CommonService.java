package com.dci.rest.service.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.rest.bdo.QueueBDO;
import com.dci.rest.model.Response;

@Service
public class CommonService {

	@Autowired
	QueueBDO commonBDO;
	
	public Response<Object> getQueuedProofsList(HttpServletRequest request) throws Exception {
		return commonBDO.getQueuedProofsList(request);
	}
	public Response<Object> getQueuedChartList(HttpServletRequest request) throws Exception {
		return commonBDO.getQueuedChartList(request);
	}
	public Response<Object> getCycleQueueDocuments(HttpServletRequest request) throws Exception {
		return commonBDO.getCycleQueueDocuments(request);
	}
	public Response<Object> getUploadQueue(HttpServletRequest request) throws Exception {
		return commonBDO.getUploadQueue(request);
	}
	
}
