package com.dci.rest.service.library;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.rest.bdo.DepartmentBDO;
import com.dci.rest.model.Response;

@Service
public class DepartmentService {

   @Autowired
   private DepartmentBDO departmentBDO;
   
	public Response<Object> getDepartmentList(HttpServletRequest request) throws Exception {
		return departmentBDO.getDepartmentList(request);
	}
}
