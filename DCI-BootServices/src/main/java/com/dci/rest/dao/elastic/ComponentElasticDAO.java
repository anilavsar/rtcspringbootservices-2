package com.dci.rest.dao.elastic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import java.text.DateFormat;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.Status;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DBXmlManager;

@Repository
@Component
public class ComponentElasticDAO extends ElasticServiceAccess {
	private Logger developerLog = Logger.getLogger(ComponentElasticDAO.class);

	@Autowired
	private Environment env;
	
	public Map<String, String> getComponentList(String queryString , String clientIndexId){		
		HttpURLConnection con=null;
		Map<String, String> result=new HashMap<String, String>();
		try {
			String URL = elServer + env.getProperty(clientIndexId).trim()+"/component/_search/template";			
			String queryDsl = queryString;
			
			developerLog.debug("ES Template --> "+URL);
			
			developerLog.debug("queryDsl--> "+queryDsl);
			
			byte[] postDataBytes = queryDsl.toString().getBytes();
			
			con = getURLConnection(URL);			
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Content-Length",String.valueOf(postDataBytes.length));
			con.setDoInput(true);
			con.setDoOutput(true);
			con.getOutputStream().write(postDataBytes);
			if (con.getResponseCode() != 200) {
				result.put("error", org.apache.commons.io.IOUtils.toString(con.getErrorStream()));	
				return result; 
		    }
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
			result.put("success", org.apache.commons.io.IOUtils.toString(br));
		    /*while ((output = br.readLine()) != null) {
		    	result.append(output);
		    }*/
		} catch (Exception e) {
			result.put("error", "Error while connecting to the server");
			new DocubuilderException("Exception in com.dci.rest.dao.elastic.ComponentElasticDAO || getComponentList()",e);
			return result;
		}finally {
			con.disconnect();
		}
		return result;
	}
	
	public Map<String, String> getFilterContent(String queryDsl, String clientIndexId) {
		HttpURLConnection con=null;
		Map<String, String> result=new HashMap<String, String>();
		try {
			String URL = elServer + env.getProperty(clientIndexId).trim()+"/component/_search/template";

			developerLog.debug("ES Template -->"+URL);
			
			developerLog.debug("queryDsl-->"+queryDsl);

			byte[] postDataBytes = queryDsl.toString().getBytes();			
			con = getURLConnection(URL);	 		
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Content-Length",String.valueOf(postDataBytes.length));
			con.setDoInput(true);
			con.setDoOutput(true);
			con.getOutputStream().write(postDataBytes);
			if (con.getResponseCode() != 200) {
				result.put("error", org.apache.commons.io.IOUtils.toString(con.getErrorStream()));	
				return result; 
		    }
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
			result.put("success", org.apache.commons.io.IOUtils.toString(br));
		} catch (Exception e) {
			e.printStackTrace();
			result.put("error", "Error while connecting to the server");
			return result;
		}finally {
			con.disconnect();
		}
		return result;
	}
	public Map<String, String> getPendingWork(String queryDsl, String clientIndexId) {
		HttpURLConnection con=null;
		Map<String, String> result=new HashMap<String, String>();
		try {
			String URL = elServer + env.getProperty(clientIndexId).trim()+"/component/_search/template";
			
			developerLog.debug("ES Template -->"+URL);
			developerLog.debug("queryDsl-->"+queryDsl);

			byte[] postDataBytes = queryDsl.toString().getBytes();			
			con = getURLConnection(URL);	 		
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Content-Length",String.valueOf(postDataBytes.length));
			con.setDoInput(true);
			con.setDoOutput(true);
			con.getOutputStream().write(postDataBytes);
			if (con.getResponseCode() != 200) {
				result.put("error", org.apache.commons.io.IOUtils.toString(con.getErrorStream()));	
				return result; 
		    }
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
			result.put("success", org.apache.commons.io.IOUtils.toString(br));
		} catch (Exception e) {
			e.printStackTrace();
			result.put("error", "Error while connecting to the server");
			return result;
		}finally {
			con.disconnect();
		}
		return result;
	}
	
	public Map<String, String> getLibraryStatistics(String queryString,String clientIndexId){		
		HttpURLConnection con=null;
		Map<String, String> result=new HashMap<String, String>();
		try {
			String URL = elStatServer + env.getProperty(clientIndexId).trim()+"/component/_search?pretty";			
			String queryDsl = queryString;
			
			developerLog.debug("ES Template -->"+URL);
			developerLog.debug("queryDsl-->"+queryDsl);

			byte[] postDataBytes = queryDsl.toString().getBytes("UTF-8");		
			con = getURLConnection(URL);
			
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			//con.setRequestProperty("reqtime", DateFormat.getCurrentDateTime());
			con.setDoOutput(true);
			con.setConnectTimeout(Integer.parseInt(reqTimeOut));
			con.getOutputStream().write(postDataBytes);
			if (con.getResponseCode() != 200) {
				result.put("error", org.apache.commons.io.IOUtils.toString(con.getErrorStream()));	
				return result; 
		    }
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
			result.put("success", org.apache.commons.io.IOUtils.toString(br));
		} catch (Exception e) {
			e.printStackTrace();
			result.put("error", "Error while connecting to the server");
			return result;
		}finally {
			con.disconnect();
		}
		return result;
	}
	
	 public  Map<String, String> httpGetDuplicateSearch(String operation, String searchTerm,boolean noDiff ,String componentId,String confidenceScore,String clientIndexId)  {
		  System.setProperty("https.protocols", "TLSv1.1");
		  String output="No Result Found";
		  StringBuffer sb= new StringBuffer("");
		  Map<String, String> result=new HashMap<String, String>();
		  HttpURLConnection conn = null;
		  int confidenceScor = 0;
		  try {
			  
				String URL = appasense + "find_duplicates_search_post/";	
				
				if( operation!=null && !operation.equals(""))
				{
					URL = appasense + "find_duplicates_search_post_suggestion/";
				}
				if( componentId != null && !componentId.equals(""))
				{
					URL = appasense + "find_duplicate_suggestion_ndiff/";
				}
				if(confidenceScore != null && !confidenceScore.equals(""))
				{
					 confidenceScor = Integer.parseInt(confidenceScore);
				}
				if( componentId == null && componentId.equals("")) {
					componentId = "";
		  		}
				developerLog.debug("URL-->"+URL);
			    URL url = new URL(URL);
				Map<String,Object> params = new LinkedHashMap<String,Object>();
		        params.put("index_name", env.getProperty(clientIndexId));
		        
		        params.put("component_searchterm", searchTerm);
		        if(noDiff){
		        	params.put("noDiff", "true");
		        }else{
		        	params.put("noDiff", "false");
		        }
		        if(componentId != "" ){
		        	params.put("component_Ids", componentId);
		        }
		        if(confidenceScor != 0){
		        	params.put("confidence_threshold", confidenceScor);
		        }
		        developerLog.debug("Index-->"+env.getProperty(clientIndexId));
		        developerLog.debug("param-->"+params);
		        StringBuilder postData = new StringBuilder();
		        for (Map.Entry<String,Object> param : params.entrySet()) {
		           if (postData.length() != 0) postData.append('&');
		            postData.append(param.getKey());
		            postData.append('=');
		            postData.append(param.getValue());
		        }
		        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
					
		        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		        Date date = new Date();
		        conn = (HttpURLConnection)url.openConnection();
		        conn.setRequestMethod("POST");
		        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		        conn.setRequestProperty("reqtime", dateFormat.format(date));
		        conn.setConnectTimeout(Integer.parseInt(reqTimeOut)); 
		        conn.setReadTimeout(Integer.parseInt(reqTimeOut));
		        conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
				
		        if (conn.getResponseCode() != 200) {
					result.put("error", org.apache.commons.io.IOUtils.toString(conn.getErrorStream()));	
					return result; 
			    }
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
				result.put("success", org.apache.commons.io.IOUtils.toString(br));
				 developerLog.debug("----Success ---");
				conn.disconnect();
			  }catch (Exception e){
				  sb.append("NO RESULT FOUND : "+e.getMessage());	
				  if(conn != null){
					  developerLog.debug("conn.disconnect(ex)");
					  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				        Date date = new Date();
				        developerLog.debug(dateFormat.format(date));
				        developerLog.debug("Request time in exception is "+conn.getRequestProperty("reqtime"));  
					  conn.disconnect();
				  }
		  		e.printStackTrace();
	  		  } 
		  return result;
		 }
	 public  Map<String, String> httpGetDuplicateByKey(String compId, int score,String clientIndexId)  {
		  System.setProperty("https.protocols", "TLSv1.1");
		  String output="No Result Found";
		  Map<String, String> result=new HashMap<String, String>();
		  StringBuffer sb= new StringBuffer("");
		  HttpURLConnection conn = null;
		  try {
				String URL = appasense + "find_duplicates/" +env.getProperty(clientIndexId).trim()+"/" + compId + "/" + score;	
				developerLog.debug("URL--->"+URL);
			  	
			  	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		        Date date = new Date();
		        developerLog.debug("Request time is "+dateFormat.format(date));
		        developerLog.debug("Request Valid time is "+reqTimeOut);
				URL url = new URL(URL);
				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("reqtime", dateFormat.format(date));
				conn.setConnectTimeout(Integer.parseInt(reqTimeOut)); 
		        conn.setReadTimeout(Integer.parseInt(reqTimeOut));

				if (conn.getResponseCode() != 200) {
					result.put("error", org.apache.commons.io.IOUtils.toString(conn.getErrorStream()));	
					return result; 
			    }
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
				result.put("success", org.apache.commons.io.IOUtils.toString(br));
				conn.disconnect();

			  }catch (Exception e){
				  sb.append("NO RESULT FOUND : "+e.getMessage());	
				  if(conn != null){
					  developerLog.debug("conn.disconnect(ex)"+e);
					  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				        Date date = new Date();
				        developerLog.debug(dateFormat.format(date));
				        developerLog.debug("Request time in exception is "+conn.getRequestProperty("reqtime"));  
					  conn.disconnect();
				  }
		  		e.printStackTrace();
		  	}
		  developerLog.debug(sb.toString());
		  return result;
		 }


	 

}
