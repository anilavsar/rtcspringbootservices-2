package com.dci.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.DepartmentBean;
import com.dci.rest.model.QueueBean;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DateFormat;

@Component
@Repository
public class CommonDAO extends DataAccessObject{
	
	private Logger developerLog = Logger.getLogger(CommonDAO.class);
	
	public List<QueueBean> getQueuedProofsList(String userid,String clientId) {
		MDC.put("category","com.dci.rest.dao.CommonDAO");

		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<QueueBean> queuedProofList = new ArrayList<QueueBean>();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.warn("call SP5_RTC_PRGETPROOFQUEUELIST("+userid+","+clientId+")");	
			cs = con.prepareCall("{call SP5_RTC_PRGETPROOFQUEUELIST(?,?)}");
			cs.setString(1,userid);
			cs.setInt(2, CheckString.getInt(clientId));
			rs = cs.executeQuery();
			int index = 0;
			while (rs != null && rs.next()) {
				QueueBean queuedProofs = new QueueBean();
				queuedProofs.setId(rs.getInt("FBOOKINSTANCEID"));
				queuedProofs.setName(rs.getString("BOOK_NAME"));
				queuedProofs.setType(rs.getString("FBOOKTYPE"));
				queuedProofs.setStatusDesc(rs.getString("CURRENT_DOC_STATUS"));//for document status
				queuedProofs.setProofStatusDesc(rs.getString("PROOF_STATUS"));//for proof status
				queuedProofs.setProofStatus(rs.getString("PROOF_STATUS"));
				queuedProofs.setCreator(rs.getString("FCREATEDBY"));
				queuedProofs.setEffDate(DateFormat.expandedTS(rs.getTimestamp("FTIMECREATED")));
				queuedProofs.setIndex(++index);
				queuedProofList.add(queuedProofs);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.CommonDAO || getQueuedProofsList()",e);
		}finally{
			releaseConStmts(rs, cs, con, "com.dci.rest.dao.CommonDAO || Method Name : getQueuedProofsList()");
			developerLog.debug("Exiting from com.dci.rest.dao.CommonDAO || Method Name : getQueuedProofsList() ||");
		}
		
		return queuedProofList;
	}
	
	public List<QueueBean> getQueuedChartList(String userid,String clientId,String documentId,String ChartDate,String chartToDate) {
		developerLog.debug("Entering into com.dci.rest.dao.CommonDAO || Method Name : getQueuedChartList() ||");		
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<QueueBean> proofsBeanArrayChart = new ArrayList<QueueBean>();
		try {
			if (con == null || con.isClosed())
				con = getConnection();

		Date changedChartDate=DateFormat.getDateOfString(ChartDate);
		Date changedToChartDate=DateFormat.getDateOfString(chartToDate);
		if(documentId!=null){
			developerLog.warn("{call sp_RTC_exGetChartStatusList('"+userid+"',"+clientId+","+documentId+",null)}");
			cs = con.prepareCall("{call sp_RTC_exGetChartStatusList(?,?,?,?)}");
			cs.setString(1, userid);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(documentId));
			cs.setNull(4,Types.NULL);
		}else{	
				developerLog.warn("{call SP4_RTC_EXGETCHARTSTATUSLIST ('"+userid+"',"+clientId+",null,'"+changedChartDate+"','"+changedToChartDate+"')}");
				cs = con.prepareCall("{call SP4_RTC_EXGETCHARTSTATUSLIST(?,?,?,?,?)}");
				cs.setString(1, userid);
				cs.setInt(2, CheckString.getInt(clientId));
				cs.setNull(3, Types.NULL);
				cs.setDate(4,(java.sql.Date) changedChartDate);					
				cs.setDate(5,(java.sql.Date) changedToChartDate);				
		}
		rs = cs.executeQuery();
		int index = 0;
		while(rs != null && rs.next()) {
			QueueBean chartsproofs = new QueueBean();
			chartsproofs.setName(rs.getString("DESC"));
			//chartsproofs.setType(rs.getString("TABLETYPE"));
			chartsproofs.setEffDate(DateFormat.expandedTS(rs.getTimestamp("fEffectiveDate")));
			chartsproofs.setExpDate(DateFormat.expandedTS(rs.getTimestamp("fExpirationDate")));
			chartsproofs.setStartTime(DateFormat.expandedTS(rs.getTimestamp("fStartTime")));
			chartsproofs.setId(rs.getInt("FELEMENTINSTANCEID"));
			chartsproofs.setEndTime(DateFormat.expandedTS(rs.getTimestamp("fCompletionTime")));
			chartsproofs.setProofStatus(rs.getString("fchartstatus"));
			chartsproofs.setType(rs.getString("fcharttype"));
			chartsproofs.setProofStatusDesc(rs.getString("fStatus_Description"));
			chartsproofs.setFileName(rs.getString("ffileref"));	
			chartsproofs.setBookDetailId(rs.getInt("fBookDetailId"));	
			chartsproofs.setBookInstanceID(rs.getInt("fBookInstanceId"));	
			chartsproofs.setDocumentName(rs.getString("fBookInstance_Description"));
			chartsproofs.setSection(rs.getString("section"));
			chartsproofs.setParentId(rs.getInt("fParentId"));
			if(rs.getString("error_desc") != null){
				chartsproofs.setErrorDec(rs.getString("error_desc"));
			}else{
				chartsproofs.setErrorDec("No Error Description");
			}
			chartsproofs.setIndex(++index);
			proofsBeanArrayChart.add(chartsproofs);
			}
		} catch(Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.CommonDAO || getQueuedChartList()",e);
		}finally {
			releaseConStmts(rs, cs, con, "com.dci.rest.dao.CommonDAO || Method Name : getQueuedChartList()");
			developerLog.debug("Exiting from com.dci.rest.dao.CommonDAO || Method Name : getQueuedChartList() ||");
		}		
		return proofsBeanArrayChart;
	}
	
	
	public List<QueueBean> getCycleQueueDocuments(String userid,String clientId) {
		MDC.put("category","com.dci.rest.dao.CommonDAO");
		developerLog.debug("Entering into com.dci.rest.dao.CommonDAO || Method Name : getCycleQueueDocuments() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<QueueBean> cycleQueueList = new ArrayList<QueueBean>();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.warn("call SP8_RTC_DTGETCYCLEDOCQUEUELIST("+userid+","+clientId+",null,null)");	
			cs = con.prepareCall("{call SP8_RTC_DTGETCYCLEDOCQUEUELIST(?,?,?,?)}");
			cs.setString(1,userid);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setDate(3,null);
			cs.setDate(4,null);
			rs = cs.executeQuery();
			int index = 0;
			while (rs != null && rs.next()) {
				QueueBean cycleQueue = new QueueBean();
				cycleQueue.setId(rs.getInt("FBOOKINSTANCEID"));
				cycleQueue.setName(rs.getString("FBOOKINSTANCE_DESCRIPTION"));
				cycleQueue.setType(rs.getString("FBOOKTYPE_DISPLAY"));
				cycleQueue.setStatusDesc(rs.getString("FSTATUS_DESCRIPTION"));//for document status
				cycleQueue.setProofStatus(rs.getString("FBOOKSTATUS"));//for proof status
				cycleQueue.setCreator(rs.getString("FLASTCHANGEDBY"));
				cycleQueue.setEffDate(DateFormat.expandedTS(rs.getTimestamp("FTIMECREATED")));				
				cycleQueue.setIndex(++index);
				cycleQueueList.add(cycleQueue);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.CommonDAO || getCycleQueueDocuments()",e);
		}finally{
				releaseConStmts(rs, cs, con, "com.dci.rest.dao.CommonDAO || Method Name : getCycleQueueDocuments()");
				developerLog.debug("Exiting from com.dci.rest.dao.CommonDAO || Method Name : getCycleQueueDocuments() ||");
		}
		return cycleQueueList;
	}
	public List<QueueBean> getImportUploadQueueList(String userId, String clientId, String subdate,  String toDate) {
		MDC.put("category"," com.dci.rest.dao");
		developerLog.debug("Entering into com.dci.rest.dao.CommonDAO || Method Name : getImportUploadQueueList() ||");
		List<QueueBean> uploadQuantList = new ArrayList<QueueBean>();
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		QueueBean uploadQuantListObj = null;
		 try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{CALL SP4_RTC_IMGETIMPORTLISTWITHDATE("+userId+","+clientId+","+subdate+","+toDate+")}");
			cs = con.prepareCall("{CALL SP4_RTC_IMGETIMPORTLISTWITHDATE(?,?,?,?)}");
			cs.setString(1, userId);
			cs.setString(2, clientId);
			cs.setDate(3, DateFormat.getDateOfString(subdate));
			cs.setDate(4, DateFormat.getDateOfString(toDate));
			rs = cs.executeQuery();
			int index=0;
			while(rs.next()) {
				uploadQuantListObj = new QueueBean();
				uploadQuantListObj.setId(rs.getInt("fimportstatusId"));
				uploadQuantListObj.setStartTime(DateFormat.expandedTS(rs.getTimestamp("fStartTime")));
				uploadQuantListObj.setEndTime(DateFormat.expandedTS(rs.getTimestamp("fCompletionTime")));
				uploadQuantListObj.setEffDate(DateFormat.expandedTS(rs.getTimestamp("fTimeCreated")));
				uploadQuantListObj.setCreator(rs.getString("fCreatedby"));
				uploadQuantListObj.setFileName(rs.getString("fImportFileLink"));
				uploadQuantListObj.setProofStatus(rs.getString("fIMPORTStatus"));
				uploadQuantListObj.setErrorDec(rs.getString("ferrortext"));
				uploadQuantListObj.setIndex(++index);
				uploadQuantList.add(uploadQuantListObj);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.CommonDAO || getImportUploadQueueList()",e);
		} finally {
			releaseConStmts(rs, cs, con, "com.dci.rest.dao.CommonDAO || Method Name : getImportUploadQueueList()");
			developerLog.debug("Exiting into com.dci.rest.dao.CommonDAO || Method Name : getImportUploadQueueList() ||");
		}
		return uploadQuantList;
	}

	public List<DepartmentBean> getDepartmentList(){
		MDC.put("category"," com.dci.rest.dao");
		developerLog.debug("Entering into com.dci.rest.dao.CommonDAO || Method Name : getDepartmentList() ||");
		List<DepartmentBean> departmentList = new ArrayList<>();
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		DepartmentBean departmentBean=null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();	
			developerLog.debug("{CALL SP_UTGETCLIENTLIST(null)}");
			cs = con.prepareCall("{CALL SP_UTGETCLIENTLIST(?)}");
			cs.setNull(1,Types.NULL);
			rs = cs.executeQuery();
			while(rs.next()) {
				departmentBean=new DepartmentBean();
				if(!rs.getString("FCLIENTID").startsWith("-")) {
					departmentBean.setDeptId(rs.getString("FCLIENTID"));
					departmentBean.setDeptName(rs.getString("FCLIENT_NAME"));
					departmentList.add(departmentBean);
				}
			}
		} catch (SQLException e) {
			new DocubuilderException("Exception in om.dci.rest.dao.CommonDAO || getDepartmentList()",e);
		}finally {
			releaseConStmts(rs, cs, con, "com.dci.rest.dao.CommonDAO || Method Name : getDepartmentList()");
			developerLog.debug("Exiting into com.dci.rest.dao.CommonDAO || Method Name : getDepartmentList() ||");
		}
		return departmentList;
	}
	
}
