package com.dci.rest.model;

import javax.validation.constraints.NotNull;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dci.rest.model.DciValidation.ValidationCreateComp;
import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SearchCriteria implements DciValidation{
	private String searchText;
	private String usageFlag;
	private String exactSearchFlag;
	private String effDate;
	private String expDate;
	private String compType;
	private String status;
	private String assetClass;
	private String funds;
	private String docType;
	private String documents;
	private String createdBy;
	private String lastChangedBy;
	private String context;
	private String locale;	
	private String count;	
	private String orAndParam;
	@NotNull(groups = {ValidationSearchLinkage.class})
	private String isFromPrimary;
	private String ComponentId;
	private String workFlowStatus;
	private String fcomp_assignee;
	private String fimportstatusid;
	private String elementInstanceIds;
	private String fTagId;
	private String createdDate;
	private String isNew;
	private String fowner;
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getUsageFlag() {
		return usageFlag;
	}
	public void setUsageFlag(String usageFlag) {
		this.usageFlag = usageFlag;
	}
	public String getExactSearchFlag() {
		return exactSearchFlag;
	}
	public void setExactSearchFlag(String exactSearchFlag) {
		this.exactSearchFlag = exactSearchFlag;
	}
	public String getEffDate() {
		return effDate;
	}
	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getCompType() {
		return compType;
	}
	public void setCompType(String compType) {
		this.compType = compType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAssetClass() {
		return assetClass;
	}
	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}
	public String getFunds() {
		return funds;
	}
	public void setFunds(String funds) {
		this.funds = funds;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getDocuments() {
		return documents;
	}
	public void setDocuments(String documents) {
		this.documents = documents;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getLastChangedBy() {
		return lastChangedBy;
	}
	public void setLastChangedBy(String lastChangedBy) {
		this.lastChangedBy = lastChangedBy;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}	
	public final String getOrAndParam() {
		return orAndParam;
	}
	public final void setOrAndParam(String orAndParam) {
		this.orAndParam = orAndParam;
	}
	public final String getIsFromPrimary() {
		return isFromPrimary;
	}
	public final void setIsFromPrimary(String isFromPrimary) {
		this.isFromPrimary = isFromPrimary;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getComponentId() {
		return ComponentId;
	}
	public void setComponentId(String componentId) {
		ComponentId = componentId;
	}
	public String getWorkFlowStatus() {
		return workFlowStatus;
	}
	public void setWorkFlowStatus(String workFlowStatus) {
		this.workFlowStatus = workFlowStatus;
	}
	public String getFcomp_assignee() {
		return fcomp_assignee;
	}
	public void setFcomp_assignee(String fcomp_assignee) {
		this.fcomp_assignee = fcomp_assignee;
	}
	public String getFimportstatusid() {
		return fimportstatusid;
	}
	public void setFimportstatusid(String fimportstatusid) {
		this.fimportstatusid = fimportstatusid;
	}
	public String getElementInstanceIds() {
		return elementInstanceIds;
	}
	public void setElementInstanceIds(String elementInstanceIds) {
		this.elementInstanceIds = elementInstanceIds;
	}
	public String getfTagId() {
		return fTagId;
	}
	public void setfTagId(String fTagId) {
		this.fTagId = fTagId;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getIsNew() {
		return isNew;
	}
	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}
	public String getFowner() {
		return fowner;
	}
	public void setFowner(String fowner) {
		this.fowner = fowner;
	}


}
