package com.dci.rest.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class AssetClass {
	private String id;
	private String name;
	
	public AssetClass(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	} 
	
	public String getId() {
		return id;
	}	
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "id is "+this.getId() + ", name - " +this.getName();
	}
}
