package com.dci.rest.model;

public class Mail {
	
	private String subject;
	private String attachment;
	private String recipient;
	private String recipientCC;
	private String recipientBCC;
	private String content;
	private String from;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getRecipientCC() {
		return recipientCC;
	}
	public void setRecipientCC(String recipientCC) {
		this.recipientCC = recipientCC;
	}
	public String getRecipientBCC() {
		return recipientBCC;
	}
	public void setRecipientBCC(String recipientBCC) {
		this.recipientBCC = recipientBCC;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}

	

}
