package com.dci.rest.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Transition {
	
	private int id;	
	private int sId;	
	private String name;
	private String asssingee;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getsId() {
		return sId;
	}
	public void setsId(int sId) {
		this.sId = sId;
	}
	public String getAsssingee() {
		return asssingee;
	}
	public void setAsssingee(String asssingee) {
		this.asssingee = asssingee;
	}


}
