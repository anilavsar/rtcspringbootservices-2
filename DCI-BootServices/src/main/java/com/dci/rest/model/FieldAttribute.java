
package com.dci.rest.model;
import java.util.*;

@SuppressWarnings("rawtypes")
public class FieldAttribute implements Comparator {
	private String name = null;
	private String value = null;

	public int compare(Object obj1, Object obj2){
		if (((FieldAttribute)obj2).name.compareTo(((FieldAttribute)obj2).name) == 0)
			return ((FieldAttribute)obj2).value.compareTo(((FieldAttribute)obj2).value);
		else
			return ((FieldAttribute)obj2).name.compareTo(((FieldAttribute)obj2).name);	
	}
	
	public FieldAttribute() {
	}
	
	public FieldAttribute(String name, String value) {
		this.name = name;
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public String getValue() {
		return value;
	}
	public void setName(String string) {
		name = string;
	}

	public void setValue(String string) {
		value = string;
	}
	
	public boolean equals(Object obj){
		if (!(obj instanceof FieldAttribute ) )
			return false;
		if (this.name.equals(((FieldAttribute)obj).name) && this.value.equals(((FieldAttribute)obj).value))	
			return true;	
		else 
			return false;
	}
	
	public int hashCode() {
		return this.name.hashCode();
	}

}
