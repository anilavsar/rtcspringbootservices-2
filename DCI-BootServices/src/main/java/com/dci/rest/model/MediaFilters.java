package com.dci.rest.model;



import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MediaFilters {
	
	private List<KeyValueModel> categoryType;
	
	private List<KeyValueModel> orientation;

	private List<KeyValueModel> funds;

	private List<KeyValueModel> gender;
	
	private List<KeyValueModel> imgtype;
	
	private List<KeyValueModel> licensing;

	private List<KeyValueModel> tags;
	
	
	
	public List<KeyValueModel> getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(List<KeyValueModel> categoryType) {
		this.categoryType = categoryType;
	}

	public List<KeyValueModel> getOrientation() {
		return orientation;
	}

	public void setOrientation(List<KeyValueModel> orientation) {
		this.orientation = orientation;
	}

	public List<KeyValueModel> getFunds() {
		return funds;
	}

	public void setFunds(List<KeyValueModel> funds) {
		this.funds = funds;
	}

	public List<KeyValueModel> getGender() {
		return gender;
	}

	public void setGender(List<KeyValueModel> gender) {
		this.gender = gender;
	}

	public List<KeyValueModel> getImgtype() {
		return imgtype;
	}

	public void setImgtype(List<KeyValueModel> imgtype) {
		this.imgtype = imgtype;
	}

	public List<KeyValueModel> getLicensing() {
		return licensing;
	}

	public void setLicensing(List<KeyValueModel> licensing) {
		this.licensing = licensing;
	}

	public List<KeyValueModel> getTags() {
		return tags;
	}

	public void setTags(List<KeyValueModel> tags) {
		this.tags = tags;
	}

}
