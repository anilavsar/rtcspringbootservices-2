package com.dci.rest.controller.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.Filter;
import com.dci.rest.model.Response;
import com.dci.rest.service.library.ComponentFilterService;
import com.dci.rest.service.library.ComponentSearchService;
@SuppressWarnings("unchecked")
@RestController
@RequestMapping({"${FILTER}","${FILTER_WITH_CLIENT}"})
@CrossOrigin
public class ComponentFilters  extends ControllerPreprocess{	
	@Autowired ComponentFilterService libFilterService;
	@Autowired ComponentSearchService searchService;
	private Logger developerLog = Logger.getLogger(ComponentFilters.class);
	
	Response<Object> result;
	
	@RequestMapping(value = "${ALL}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Response<Object> getFilterList(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.controller.library.AppasesneController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentFilters || Method Name : getFilterList() || input : "+pathVariablesMap);
		try {
			result = libFilterService.getFilterList(request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentFilters Error in  Method Name : getFilterList() :" + e,e);
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentFilters || Method Name : getFilterList() || response : "+result);
		return result;
	}
	

	@RequestMapping(value = {"${FILTER_NAME}","${FILTER_NAME_SPEC}","${FILTER_PENDING_WORK}"}, method = RequestMethod.GET, produces = "application/json" )
	public Response<Object> getFilterContent(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentFilters");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentFilters || Method Name : getFilterContent() || input : "+pathVariablesMap);
		try {
			result = searchService.getFilterContent(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentFilters || Method Name : getFilterContent() || response : "+result);
		return result;
	}
	
	@RequestMapping(value = "${CREATE_FILTER}", method = RequestMethod.POST, produces = "application/json",consumes = "application/json" )
	public Response<Object> createUserFilter(@Validated(Filter.ValidationCreateFilter.class) @RequestBody Filter filter,BindingResult bindingResult,@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentFilters");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentFilters || Method Name : createUserFilter() || input : "+pathVariablesMap +","+filter);
		try {
			
			//pathVariablesMap.put("bindingResult", bindingResult);
			//pathVariablesMap.put("filter", filter);
			result = libFilterService.createUserFilter(pathVariablesMap,request,filter,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentFilters || Method Name : createUserFilter() || response : "+result);
		return result;
	}
	@RequestMapping(value = "${EDIT_FILTER}", method = RequestMethod.POST, produces = "application/json",consumes = "application/json" )
	public Response<Object> editUserFilter(@RequestBody Filter filter,BindingResult bindingResult,@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentFilters");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentFilters || Method Name : editUserFilter() || input : "+pathVariablesMap +","+filter);
		try {
			//pathVariablesMap.put("filter", filter);
			//pathVariablesMap.put("bindingResult", bindingResult);
			result = libFilterService.editUserFilter(pathVariablesMap,request,filter,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentFilters || Method Name : editUserFilter() || response : "+result);
		return result;
	}
	
	@RequestMapping(value = "${USER_FILTERS}", method = RequestMethod.GET, produces = "application/json")
	public Response<Object> getUserFilters(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentFilters");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentFilters || Method Name : getUserFilters() || input : "+pathVariablesMap);
		try {
			result = libFilterService.getUserFilters(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentFilters || Method Name : getUserFilters() || response : "+result);
		return result;
	}
	
	@RequestMapping(value = {"${FILTER_NAME}"}, method = RequestMethod.POST, produces = "application/json" )
	public Response<Object> getFilterContent(@PathVariable Map<String, String> pathVariablesMap,@RequestBody String contextIdsJson,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentFilters");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentFilters || Method Name : getFilterContent() || input : "+pathVariablesMap);
		try {
			JSONObject obj=new JSONObject(contextIdsJson);
			String contextIds=obj.getString("contextIds");
			
			//System.out.println("contextIds@getFilterContent:"+contextIds);
			 pathVariablesMap.put("contextIds",contextIds);          
			result = searchService.getFilterContent(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentFilters || Method Name : getFilterContent() || response : "+result);
		return result;
	}
	
}
