package com.dci.rest.controller.library;



import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.bean.DashboardLayout;
import com.dci.rest.model.Response;
import com.dci.rest.service.library.DashboardService;

@RestController
@RequestMapping({"${DASHBOARD}"})
@CrossOrigin
public class DashboardController {

	@Autowired
	DashboardService dashboardService;
	
	private Logger developerLog = Logger.getLogger(DashboardController.class);

	@RequestMapping(value = "${GET_CHART_PREFERENCES}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getUserChartPrivileges(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.DashboardController");
		developerLog.debug("Entering into com.dci.rest.controller.library.DashboardController || Method Name : getUserChartPrivileges() ||  input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = dashboardService.getUserChartPrefrence(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.DashboardController || Method Name : getUserChartPrivileges() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${UPDATE_CHART_PREFERENCES}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> updateChartOrder(@PathVariable Map<String, String> pathVariablesMap,@RequestBody DashboardLayout dashboardLayout,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.DashboardController");
		developerLog.debug("Entering into com.dci.rest.controller.library.DashboardController || Method Name : updateChartOrder() ||  input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = dashboardService.updateChartPrefrence(pathVariablesMap,dashboardLayout,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.DashboardController || Method Name : updateChartOrder() || response : "+responseVO);
		return responseVO;		
	}
	
}
