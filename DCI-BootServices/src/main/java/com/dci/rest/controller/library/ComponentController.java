package com.dci.rest.controller.library;


import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ContextBean;
import com.dci.rest.model.ContextSchema;
import com.dci.rest.model.ResloveVarBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.TableComponent;
import com.dci.rest.service.library.ComponentService;
import com.dci.rest.utils.CheckString;
@RestController
@RequestMapping({"${LIBRARY}","${LIBRARY_WITH_CLIENT}"})
@CrossOrigin
public class ComponentController{
	
	private Logger developerLog = Logger.getLogger(ComponentController.class);

	@Autowired
	ComponentService compService;
	
	@RequestMapping(value = "${VALIDNAME}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> isValidDisplayName(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){	
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : isValidDisplayName() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			if(CheckString.isValidString(pathVariablesMap.get("name"))){
				responseVO = compService.isValidName(pathVariablesMap.get("name"),request);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : isValidDisplayName() || response : "+responseVO);
		return responseVO;
		
	}
	@RequestMapping(value = "${CREATE_NEW}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createComponent(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request,@Validated(ComponentBean.ValidationCreateComp.class)  @RequestBody ComponentBean component,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createComponent() || input : "+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			//pathVariablesMap = setUserDetails(pathVariablesMap);
			//pathVariablesMap.put("request", request);
			responseVO = compService.createComponent(component, pathVariablesMap, bindingResult,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : createComponent() || response : "+responseVO);
		return responseVO;		
	}
	
	@RequestMapping(value = "${CREATE_TABLE}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createTableComponent(@Validated(TableComponent.ValidationCreateTableComp.class)  @RequestBody TableComponent table,BindingResult bindingResult, @PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createTableComponent() || input : "+pathVariablesMap + ","+table);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.createTableComponent(table,bindingResult,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : createTableComponent() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${EDIT}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editComponent(@PathVariable Map<String, Object> pathVariablesMap,@RequestBody ComponentBean component, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : editComponent() || input : "+pathVariablesMap+ ","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.editComponent(pathVariablesMap.get("idToEdit").toString(), component,pathVariablesMap,request);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : editComponent() || response : "+responseVO);
		return responseVO;	
	}
	@RequestMapping(value = "${EDIT_TABLE}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editTableComponent(@PathVariable Map<String,Object> pathVariablesMap,@RequestBody TableComponent table,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : editTableComponent() || input : "+pathVariablesMap+ ","+table);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.editTableComponent(pathVariablesMap.get("tableId").toString(), table,pathVariablesMap,request);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : editTableComponent() || response : "+responseVO);
		return responseVO;	
	}
	@RequestMapping(value="${GET_COMPONENT}", method=RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE )
	public Response<Object> getComponentData(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentData() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getComponent(pathVariablesMap.get("componentId").toString(), pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentData() || response : "+responseVO);
		return  responseVO;
	}
	
	@RequestMapping(value="${GET_CONTEXTLIST}", method=RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE )
	public Response<Object> getContextList(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request) {	
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getContextList() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getContextList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getContextList() || response : "+responseVO);
		return responseVO;
	}
	@RequestMapping(value="${GET_LOCALLIST}", method=RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE )
	public Response<Object> getLocalList(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getLocalList() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO=compService.getLocalList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getLocalList() || response : "+responseVO);
		return responseVO;
	}
	
	@RequestMapping(value = "${GET_ASSOCIATION_DATA}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAssociationData(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getAssociationData() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getAssociationData(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getAssociationData() || response : "+responseVO);
		return  responseVO;
	}

	@RequestMapping(value = "${GET_FUNDS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getFundList(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getFundList() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
					responseVO = compService.getFundList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getFundList() || response : "+responseVO);
		return  responseVO;
	}
	
	@RequestMapping(value = "${GET_DOCUMENTS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getDocumentList(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getDocumentList() ||  input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getDocumentList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getDocumentList() || response : "+responseVO);
		return responseVO;
	}

	@RequestMapping(value = "${GET_DOCUMENTTYPE}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getDocumentTypeList(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getDocumentTypeList() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getDocumentTypeList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getDocumentTypeList() ||response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${GET_CREATED_UPDATED_BY_USERS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getCreatedByUser(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getCreatedByUser() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getCreatedUpdatedBy(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getCreatedByUser() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${GET_STATUS_LIST}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getStatusList(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getStatusList() || input : "+pathVariablesMap);

		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getStatusList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getStatusList() || response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${GET_ASSETCLASSLIST}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAssetList(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getAssetList() ||  input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();		
		try {
			responseVO = compService.getAssetList(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getAssetList() ||response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${SET_ASSOCIATION}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> setComponentAssociation(@PathVariable Map<String, Object> pathVariablesMap,@RequestBody ComponentBean component, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : setComponentAssociation() || input : "+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.setComponentAssociation(pathVariablesMap.get("componentId").toString(), component, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : setComponentAssociation() ||response : "+responseVO);
		return  responseVO;
	}
	@RequestMapping(value = "${GET_ASSOCIATION}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getComponentAssociation(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentAssociation() || input : "+pathVariablesMap);

		Response<Object> responseVO = new Response<Object>();		
		try {
			responseVO = compService.getComponentAssociation(pathVariablesMap.get("componentId").toString(), request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentAssociation() || response : "+responseVO);
		return responseVO;
	}
	@RequestMapping(value = "${GET_COMMENTS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getComponentComments(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentComments() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		 try {
			responseVO = compService.getComponentComments(pathVariablesMap.get("componentId").toString(),request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentComments() || response : "+responseVO);
		return responseVO;	
	}
	@RequestMapping(value ="${ADD_COMMENT}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createComment(@PathVariable Map<String, Object> pathVariablesMap,@Validated(ComponentBean.ValidationCreateComment.class) @RequestBody ComponentBean component,BindingResult result, HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createComment() || input :"+pathVariablesMap +","+component);
		Response<Object> responseVO = new Response<Object>();	
		try {
			responseVO = compService.createComponentComment(pathVariablesMap.get("componentId").toString(),component,request,result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : createComment() ||response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${GET_LINKAGE_COMPONENT}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getLinkageComponent(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getLinkageComponent() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getLinkageComponent(pathVariablesMap.get("sourceCompId").toString(),request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getLinkageComponent() || response : "+responseVO);
		return responseVO;	
	}
	@RequestMapping(value = "${PROCESS_COMP_PRIMARY_FLAG}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> processComponentPrimaryFlag(@PathVariable Map<String, Object> pathVariablesMap,@PathVariable String flag, HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : processComponentPrimaryFlag() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.processComponentPrimaryFlag(pathVariablesMap.get("componentId").toString(),flag,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : processComponentPrimaryFlag() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${GET_COMPONENT_VERSIONS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getComponentHistoryData(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request){
		MDC.put("input",pathVariablesMap);
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentHistoryData() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();		
		try {
			responseVO = compService.getComponentHistoryData(pathVariablesMap.get("componentId").toString(),request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentHistoryData() ||");
		return responseVO;		
	}
		@RequestMapping(value = "${ADD_LINKS}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> linkComponents(@PathVariable Map<String, Object> pathVariablesMap,@RequestBody ComponentBean component, HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : linkComponents() || input : "+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.linkComponents(pathVariablesMap.get("componentId").toString(),component,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : linkComponents() || response : "+responseVO);
		return responseVO;		
	}
	
	@RequestMapping(value = "${REMOVE_LINKS}", method = RequestMethod.POST)
	public Response<Object> disassociateSecondaryComonent(@PathVariable Map<String, Object> pathVariablesMap,@RequestBody ComponentBean component,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : disassociateSecondaryComonent() || input : "+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.disassociateSecondaryComonent(pathVariablesMap.get("componentId").toString(),component,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : disassociateSecondaryComonent() || response : "+responseVO); 
		return responseVO;		
	}
	@RequestMapping(value = "${GET_COMPONENT_PRIVILEGE}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getComponentPrivilege(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentPrivilege() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getComponentPrivilege(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentPrivilege() || response : "+responseVO);
		return responseVO;		
	} 
	@RequestMapping(value = "${DEACTIVATE_COMPONENT}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> deactivateComponent(@PathVariable Map<String, Object> pathVariablesMap,@Validated(ComponentBean.ValidationDeactivateComp.class) @RequestBody ComponentBean component,BindingResult bindingresult,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : deactivateComponent() || input : "+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.deactivateComponent(component,bindingresult,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : deactivateComponent() || response : "+responseVO);
		return responseVO;		
	} 
	@RequestMapping(value = "${BLACKLINE}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getComponentBlackline(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentBlackline() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getComponentBlackline(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentBlackline() || response : "+responseVO);
		return responseVO;		
	} 
	@RequestMapping(value = "${BLACKLINE_ON_CREATE_COMP}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getCreateComponentBlackline(HttpServletRequest request, @RequestBody Map<String, String> payload){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getCreateComponentBlackline() ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getCreateComponentBlackline(payload,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getCreateComponentBlackline() || response : "+responseVO);
		return responseVO;		
	} 
	@RequestMapping(value = "${VERSION_BLACKLINE}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getVersionBlackline(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getVersionBlackline() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getVersionBlackline(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getVersionBlackline() || response : "+responseVO);
		return responseVO;		
	} 
	@RequestMapping(value = {"${VARIABLES}","${VARIABLES_SPEC}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getVariables(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getVariables() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getVariables(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getVariables() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${IMAGES}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getImages(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getImages() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getImages(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getImages() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = {"${INLINESTYLE}","${INLINESTYLE_SPECIFIC}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getInlineStyles(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getInlineStyles() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getInlineStyles(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getInlineStyles() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${ADD_TO_FAVORITE}", method = RequestMethod.POST)
	public Response<Object> addToFavoriteLibrary(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : addToFavoriteLibrary() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.addToFavoriteLibrary(pathVariablesMap.get("selectedComponents").toString(),request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : addToFavoriteLibrary() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${GET_LIBRARY_FAVORITE}", method = RequestMethod.GET)
	public Response<Object> getLibraryFavorite(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getLibraryFavorite() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getLibraryFavorite(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getLibraryFavorite() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${REMOVE_LIBRARY_FAVORITE}", method = RequestMethod.DELETE)
	public Response<Object> removeFromFavoriteLibrary(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : removeFromFavoriteLibrary() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.removeFromFavoriteLibrary(pathVariablesMap.get("selectedComponents").toString(),request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : removeFromFavoriteLibrary() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = {"${GET_COMPONENT_TAGS}","${GET_ALL_TAGS}"}, method = RequestMethod.GET)
	public Response<Object> getComponentTags(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentTags() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getComponentTags(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentTags() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = {"${CREATE_TAGS}","${CREATE_ASSOCIATE_TAG}"}, method = RequestMethod.POST)
	public Response<Object> createTags(@RequestBody ComponentBean component, @PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createTags() || input : "+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.createTags(pathVariablesMap,component,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : createTags() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${SET_COMPONENT_TAGS}", method = RequestMethod.POST)
	public Response<Object> setComponentTags(@PathVariable Map<String, String> pathVariablesMap,@Validated(ComponentBean.ValidationSetTag.class) @RequestBody ComponentBean component,BindingResult bindingResult,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : setComponentTags() || input :"+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.setComponentTags(component,bindingResult,pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : setComponentTags() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${CREATE_LINKAGE_COMPONENT}", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createLinkageComponent(@PathVariable Map<String, String> pathVariablesMap,@Validated(ComponentBean.ValidationCreateLinkageComponent.class) @RequestBody ComponentBean component,BindingResult bindingResult,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createLinkageComponent() || input :"+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.createLinkageComponent(component,bindingResult,pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : createLinkageComponent() || response : "+responseVO);
		return responseVO;		
	}
	
	@RequestMapping(value = {"${GET_TABLE_TYPES}","${GET_TABLE_TYPES_DOC_SPEC}"}, method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getTableTypes(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getTableTypes() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getTableTypes(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getTableTypes() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = {"${GET_TABLE_ROW_STYLE}"}, method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getElementStylesList(@PathVariable Map<String, String> pathVariablesMap,@PathVariable String type,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getElementStylesList() ||  input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getElementStylesList(request,type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getElementStylesList() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${GET_COMP_TYPE}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getCompType(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getCompType() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getCompType(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getCompType() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${UPDATE_SECONDARY_COMP_STATUS}", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> updateSecondaryCompStatus(@PathVariable Map<String, Object> pathVariablesMap,HttpServletRequest request,@Validated(ComponentBean.ValidateStatusId.class) @RequestBody ComponentBean component,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : updateSecondaryCompStatus() || input : "+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.updateSecondaryCompStatus(pathVariablesMap.get("componentId").toString(),component,request,bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : updateSecondaryCompStatus() || response : "+responseVO); 
		return responseVO;		
	}
	
	@RequestMapping(value = "${CREATE_CONTEXT}", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createContext(@Validated(ContextBean.ValidationCreateContext.class)@RequestBody ContextBean contextBean,BindingResult bindingResult, @PathVariable Map<String, String> pathVariablesMap, HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createContext() || input : "+pathVariablesMap + ","+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.createContext(contextBean,request, bindingResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : createContext() || response : "+responseVO);
		return responseVO;		
	}
	@RequestMapping(value = "${EDIT_CONTEXT}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editContext( @PathVariable Map<String, Object> pathVariablesMap,@Validated(ContextBean.ValidationCreateContext.class)@RequestBody ContextBean contextBean,BindingResult bindingResult, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : editComponent() || input : "+pathVariablesMap+ ","+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.editContext(pathVariablesMap.get("contextId").toString(), contextBean,bindingResult,pathVariablesMap,request);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : editComponent() || response : "+responseVO);
		return responseVO;	
	}
	
	@RequestMapping(value = "${GET_FOOTNOTE_LIST}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getFootnoteList( @PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getFootnoteList() || input : "+pathVariablesMap+ ",");
		Response<Object> responseVO = new Response<Object>();
		try {
 			responseVO = compService.getFootnoteList(pathVariablesMap,request);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getFootnoteList() || response : "+responseVO);
		return responseVO;	
	}
	
	@RequestMapping(value = "${GET_FUND_LIST_ON_ASSETCLASS}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> fundListonAssetClass( @PathVariable Map<String, Object> pathVariablesMap,@Validated(ComponentBean.ValidationGetFundByAsset.class)@RequestBody ComponentBean componentBean, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : fundListonAssetClass() || input : "+pathVariablesMap+ ",");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.fundListonAssetClass(pathVariablesMap,componentBean,request);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : fundListonAssetClass() || response : "+responseVO);
		return responseVO;	
	}
	@RequestMapping(value = {"${GET_NEW_STATUS_LIST_ALL}","${GET_NEW_STATUS_LIST}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getComponentStatusList(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getComponentStatusList() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try { 
			responseVO = compService.getComponentStatusList(pathVariablesMap,request);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getComponentStatusList() || response : "+responseVO);
		return responseVO;	
	}	
	@RequestMapping(value = "${GET_AUTH_USERS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAuthUsers(HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getAuthUsers() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try { 
			responseVO = compService.getAuthUsers(request);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getAuthUsers() || response : "+responseVO);
		return responseVO;	
	}	
	
	@RequestMapping(value = "${BULK_DATA_UPDATE}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> bulkdataUpdate(HttpServletRequest request, @RequestBody ComponentBean component) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : bulkdataUpdate() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try { 
			responseVO = compService.bulkdataUpdate(request,component);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : bulkdataUpdate() || response : "+responseVO);
		return responseVO;	
	}	
	
	@RequestMapping(value = "${GET_UPLOAD_UPDATED}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAffectedComponent(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getAffectedComponent() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.getAffectedComponent(pathVariablesMap.get("fimportstatusid"),request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : getAffectedComponent() :"+e,e);
		}
		return responseVO;		
	}
	@RequestMapping(value = "${REVERT_TO_VERSIONS}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> revertToVersion(HttpServletRequest request,@RequestBody ComponentBean component,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : revertToVersion() || input : "+component);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.revertToVersion(component,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : revertToVersion() :"+e,e);
		}
		return responseVO;		
	}	
	@RequestMapping(value = {"${CREATE_PRIMARY_CATEGORY}","${CREATE_SUB_CATEGORY}"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createPrimarySecondaryCatogory(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request,@RequestBody ContextBean contextBean,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createPrimarySecondaryCatogory() || input : "+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.createPrimarySecondaryCatogory(pathVariablesMap,contextBean,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : createPrimarySecondaryCatogory() :"+e,e);
		}
		return responseVO;		
	}
	@RequestMapping(value = {"${REMOVE_CATEGORY}"}, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> removeCategory(HttpServletRequest request,@RequestBody ContextBean contextBean,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createPrimarySecondaryCatogory() || input : "+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.removeCategory(contextBean,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : createPrimarySecondaryCatogory() :"+e,e);
		}
		return responseVO;		
	}
	
	// need to move to different Controller ..Common Methods 
	@RequestMapping(value = "${ADD_PERMISSION_TO_CATEGORY}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> addPermissionToContentCategory(HttpServletRequest request,@RequestBody ContextBean contextBean,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : addPermissionToContentCategory() || input : "+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.addPermissionToContentCategory(contextBean,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : addPermissionToContentCategory() :"+e,e);
		}
		return responseVO;		
	}	
	
/*	// need to move to different Controller ..Common Methods 
	@RequestMapping(value = "${EDIT_AUTH_SUB_CATEGORY}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> authEditSubContentCatogory(HttpServletRequest request,@RequestBody ContextBean contextBean,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.CommonController");
		developerLog.debug("Entering into com.dci.rest.controller.library.CommonController || Method Name : authEditSubContentCatogory() || input : "+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.authEditSubContentCatogory(contextBean,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.CommonController || Method Name : authEditSubContentCatogory() :"+e,e);
		}
		return responseVO;		
	}*/
	
	@RequestMapping(value = "${CREATE_CATEGORY_SCHEMA}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> createCategorySchema(HttpServletRequest request,@RequestBody ContextSchema contextSchema,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createCategorySchema() || input : "+contextSchema);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.createCategorySchema(contextSchema,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : createCategorySchema() :"+e,e);
		}
		return responseVO;		
	}
	@RequestMapping(value = "${EDIT_CATEGORY}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editContentCatogory(HttpServletRequest request,@RequestBody ContextBean contextBean,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createContentCatogory() || input : "+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.editContentCatogory(contextBean,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : createContentCatogory() :"+e,e);
		}
		return responseVO;		
	}	
	@RequestMapping(value = "${EDIT_SUB_CATEGORY}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> editSContentCatogory(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request,@RequestBody ContextBean contextBean,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : createContentCatogory() || input : "+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.editSubContentCatogory(pathVariablesMap,contextBean,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : createContentCatogory() :"+e,e);
		}
		return responseVO;		
	}
	@RequestMapping(value = "${ORDER_SUB_CATEGORY}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> orderContentCatogory(HttpServletRequest request,@RequestBody ContextBean contextBean,BindingResult bindingResult){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : orderContentCatogory() || input : "+contextBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.orderContentCatogory(contextBean,request,bindingResult);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : orderContentCatogory() :"+e,e);
		}
		return responseVO;		
	}
	@RequestMapping(value = {"${GET_CONETNT_CATEGORY_ALL}","${GET_CONETNT_CATEGORY}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getContentCategory(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getContentCategory() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.getContentCategory(pathVariablesMap,request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : getContentCategory() :"+e,e);
		}
		return responseVO;		
	}
	@RequestMapping(value = {"${GET_CONTENT_CATEGORY_WITH_PERMISSION}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getContentCatByGroup(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getContentCategory() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.getContentCatByGroup(pathVariablesMap,request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : getContentCategory() :"+e,e);
		}
		return responseVO;		
	}
	
	@RequestMapping(value = {"${AVAILABLE_TO_PRIMARY_CATEGORY}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> availableToPrimary(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : availableToPrimary() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.availableToPrimary(pathVariablesMap,request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : availableToPrimary() :"+e,e);
		}
		return responseVO;		
	}

	@RequestMapping(value = "${GET_FUNDS_SHARECLASS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getShareClassByFund(@PathVariable  Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getAssetclassByFund() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
					responseVO = compService.getShareClassByFund(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getAssetclassByFund() || response : "+responseVO);
		return  responseVO;
	}
	
	
	@RequestMapping(value = "${RESOLVE_COMPONENT_VARIABLE}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> resolveComponentVariables(HttpServletRequest request,@PathVariable  Map<String, String> pathVariablesMap,@RequestBody ResloveVarBean resloveVarBean){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : resolveComponentVariables() || input : "+resloveVarBean);
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.resolveComponentVariables(pathVariablesMap,resloveVarBean,request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : resolveComponentVariables() :"+e,e);
		}
		return responseVO;		
	}	
	
	
	@RequestMapping(value = "${GET_RESOLVE_COMPONENT_VARIABLE}",  method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> previewVariableValue(@PathVariable  Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : previewVariableValue() ||");
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.previewVariableValue(pathVariablesMap,request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : previewVariableValue() :"+e,e);
		}
		return responseVO;		
	}	
	
	@RequestMapping(value = "${GET_ALL_COMPONENTS_FOR_CART}",  method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getAllComponentsForCart(@PathVariable  Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getAllComponentsForCart() ||");
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.getAllComponentsForCart(pathVariablesMap,request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : getAllComponentsForCart() :"+e,e);
		}
		return responseVO;
	}
	
	
	@RequestMapping(value = "${SAVE_All_COMPONENTS_TO_CART}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> saveAllComponentsToCart(@PathVariable  Map<String, String> pathVariablesMap,HttpServletRequest request,@RequestBody ComponentBean componentBean) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : saveAllComponentsToCart() ||");
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.saveAllComponentsToCart(pathVariablesMap.get("userid"),pathVariablesMap.get("clientid"),componentBean,request);
			
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : saveAllComponentsToCart() :"+e);
		}
		return responseVO;	
	}
 
	@RequestMapping(value = "${REMOVE_COMPONENTS_FROM_CART}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> removeComponentsFromCart(@PathVariable  Map<String, String> pathVariablesMap,HttpServletRequest request,@RequestBody ComponentBean componentBean) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : removeComponentsFromCart() ||");
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = compService.removeComponentsFromCart(pathVariablesMap.get("userid"),pathVariablesMap.get("clientid"),componentBean,request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : removeComponentsFromCart() :"+e);
		}
		return responseVO;		
	}
	@ResponseBody
	@RequestMapping(value="${GET_IMAGE}", method=RequestMethod.GET, produces= MediaType.IMAGE_JPEG_VALUE)
	public void getImageData(@PathVariable Map<String, Object> pathVariablesMap, HttpServletRequest request, HttpServletResponse response) {
		MDC.put("category","com.dci.rest.controller.library.ComponentController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentController || Method Name : getImageData() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = compService.getImage(pathVariablesMap.get("imageId").toString(), pathVariablesMap,request);
			if(responseVO.getStatus().getStatusCode()==200 && ((ComponentBean)responseVO.getResult()).getImageByteArray().length>0) {
				response.setContentType("image/jpg");
				ServletOutputStream out = response.getOutputStream();
				developerLog.debug("Image Byte Array Size::"+((ComponentBean)responseVO.getResult()).getImageByteArray().length);
				out.write(((ComponentBean)responseVO.getResult()).getImageByteArray());
				developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getImageData()");
			}else {
				developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentController || Method Name : getImageData() || response : File not found");
				response.setContentType("text/xml");
				response.getWriter().write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><data>not found</data>");
				response.getWriter().flush();
				response.getWriter().close();
			}	
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.ComponentController || Method Name : getImageData() :"+e);
		}
	}
}
