package com.dci.rest.controller.library;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.AppaSlide;
import com.dci.rest.model.Response;
import com.dci.rest.service.library.AppaSlidesService;

@RestController
@RequestMapping({ "${APPASLIDES}" })
@CrossOrigin
public class AppaSlidesController {

	private Logger developerLog = Logger.getLogger(AppaSlidesController.class);

	@Autowired
	AppaSlidesService appaSlideService;

	@RequestMapping(value = "${SAVESLIDEINFORMATION}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> saveAppaSlideInformation(@PathVariable Map<String, String> pathVariablesMap,
			@RequestBody List<AppaSlide> requestBody, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.controller.library.AppaSlidesController");
		developerLog.debug(
				"Entering into com.dci.rest.controller.library.AppaSlidesController || Method Name : saveAppaSlideInformation() || input : "
						+ pathVariablesMap);
		// System.out.println("Inside saveAppaSlideInformation");
		Response<Object> responseVO = new Response<Object>();

		try {
			developerLog
					.debug("Calling the appaSlideService.saveAppaSlides || Method Name : saveAppaSlideInformation()");
			responseVO = appaSlideService.saveAppaSlides(request, requestBody);
		} catch (Exception e) {
			e.printStackTrace();
		}

		developerLog.debug(
				"Exiting from com.dci.rest.controller.library.AppaSlidesController || Method Name : saveAppaSlideInformation()");
		System.out.println(responseVO.toString());
		return responseVO;
	}

	@RequestMapping(value = "${CHECKSLIDEINFORMATION}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> checkAppaSlideInformation(@PathVariable Map<String, String> pathVariablesMap,
			@RequestBody List<AppaSlide> requestBody, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.controller.library.AppaSlidesController");
		developerLog.debug(
				"Entering into com.dci.rest.controller.library.AppaSlidesController || Method Name : saveAppaSlideInformation()");
		// System.out.println("Inside saveAppaSlideInformation");
		Response<Object> responseVO = new Response<Object>();

		try {
			developerLog
					.debug("Calling the appaSlideService.saveAppaSlides || Method Name : saveAppaSlideInformation()");
			responseVO = appaSlideService.checkAppaSlides(request, requestBody);
		} catch (Exception e) {
			e.printStackTrace();
		}

		developerLog.debug(
				"Exiting from com.dci.rest.controller.library.AppaSlidesController || Method Name : saveAppaSlideInformation() || response : ");
		System.out.println(responseVO.toString());
		return responseVO;
	}

}
