package com.dci.rest.exception;

import java.io.*;
import java.net.*;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileLockInterruptionException;
import java.nio.charset.*;
import java.rmi.*;

public class DocubuilderIOException {
	
	String msg = "|| Exception Details ||"+"\n"+"|| SystemException in Module Name: ";
	
	public DocubuilderIOException(Throwable ex, String module) {
		
		if(ex instanceof CharacterCodingException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof CharConversionException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof ClosedChannelException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof EOFException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof FileLockInterruptionException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof FileNotFoundException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof IOException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof InterruptedIOException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof MalformedURLException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof ObjectStreamException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof ProtocolException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof RemoteException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof SocketException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof SyncFailedException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof UnknownServiceException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof UnsupportedEncodingException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof UTFDataFormatException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else{
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}
		
	}
	
	/**
     * Prints in the log what message is returned.
     */		
	public String getMsg(){
		return msg;
	}
}
