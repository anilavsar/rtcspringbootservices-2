package com.dci.rest.exception;

public class DocubuilderSystemException {

		String msg = "|| Exception Details ||"+"\n"+"|| SystemException in Module Name: ";
		
		public DocubuilderSystemException(Throwable ex, String module) {
			
			if(ex instanceof InternalError) {
				
				msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
				
			}else if(ex instanceof OutOfMemoryError) {
				
				msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
				
			}else if(ex instanceof StackOverflowError) {
				
				msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
				
			}else if(ex instanceof UnknownError) {
				
				msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
				
			}else{
				
				msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
				
			}
			
		}
		
		public String getMsg(){
			return msg;
		}
}
