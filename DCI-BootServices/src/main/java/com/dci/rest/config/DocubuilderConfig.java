package com.dci.rest.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.tomcat.jdbc.pool.DataSource;

//import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


import com.dci.rest.common.CustomHtmlLayout;
//import com.dci.rest.dao.DataConnection;
/*import com.google.common.base.Predicate;
import static springfox.documentation.builders.PathSelectors.regex;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;*/


@Configuration
@ComponentScan(basePackages = "com.dci.rest")
//@EnableSwagger2
@PropertySources({
    @PropertySource("classpath:resources.properties"),
    @PropertySource("classpath:docubuilder.properties"),
    @PropertySource("classpath:message.properties"),
    @PropertySource("classpath:apiresources.properties"),
    @PropertySource("classpath:docubuilderclientconfig.properties")
})

public class DocubuilderConfig implements DocuBuilder{
	 @Autowired CustomHtmlLayout customHtmlLayout;
	 @Autowired private Environment environment;	
		
	 @Value("${DB_LIBRARY}" ) private String database_Library;
	 @Value("${DB_DRIVER_URL}" ) private String database_Driver_URL;
	 @Value("${DATABASE_DRIVER_NAME}" ) private String database_Driver_Name;

	 
	 	@Bean
	    public InternalResourceViewResolver viewResolver() {
	        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
	        resolver.setPrefix("/WEB-INF/jsp/");
	        resolver.setSuffix(".jsp");
	        return resolver;
	    }
	 
	    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	    }
	   
	    public void addCorsMappings(CorsRegistry registry) {
	    	String origins = environment.getProperty("ALLOW_ORIGINS");
	    	//registry.addMapping("/**").allowedOrigins(origins.split(","));
	    	registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST", "OPTIONS", "PUT" ,"DELETE");
	        registry.addMapping("http://localhost:9080");
	        registry.addMapping("http://10.162.200.61/");
	        registry.addMapping("https://*.docubuilder.com");
	    }
	    /*
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("Docubuilder API", "Docubuilder Spring-Boot library api", "1.0",
			"urn:tos", "Rohit", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");
	private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<String>(
			Arrays.asList("application/json"));
			*/
			
	 /*
	@Bean
	public Docket api() {
		//return  new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_API_INFO).produces(DEFAULT_PRODUCES_AND_CONSUMES)
			//	.consumes(DEFAULT_PRODUCES_AND_CONSUMES)).paths(postPaths()).build();
				
				return new Docket(DocumentationType.SWAGGER_2).groupName("public-api").apiInfo(apiInfo()).select()
						.paths(postPaths()).build();
	}
	
	private Predicate<String> postPaths() {
		return (regex("/DCI/.*"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Docubuilder Spring-Boot Library API")
				.version("1.0").build();
	}
	*/

		@Bean
		@Primary
		@ConfigurationProperties("db2itoolbox.spring.datasource")
		public DataSourceProperties primaryDataSourceProperties() {
		     return new DataSourceProperties();
		}
	
		
	    @Bean(name="firstdatasource")
	    @Primary
	    @ConfigurationProperties(prefix="db2itoolbox.spring.datasource")
	    public org.apache.tomcat.jdbc.pool.DataSource primaryDataSource() {
	         return (DataSource) primaryDataSourceProperties().initializeDataSourceBuilder().build();
	    }
	    
	    @Bean
		@ConfigurationProperties("db4auth.spring.datasource")
		public DataSourceProperties secondaryDataSourceProperties() {
		     return new DataSourceProperties();
		}
	
	    @Bean(name="seconddatasource")
	    @ConfigurationProperties(prefix="db4auth.spring.datasource")
	    public org.apache.tomcat.jdbc.pool.DataSource secondaryDataSource() {
	         return (DataSource) secondaryDataSourceProperties().initializeDataSourceBuilder().build();
	    }

	   
}
