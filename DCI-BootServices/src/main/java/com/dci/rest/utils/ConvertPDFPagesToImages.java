package com.dci.rest.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.dci.rest.bdo.ComponentBDO;

public class ConvertPDFPagesToImages {
	private Logger developerLog = Logger.getLogger(ComponentBDO.class);
	public boolean convertPDFToImage(String source, String destination, String format) {
		MDC.put("category","com.dci.rest.utils.ConvertPDFPagesToImages");
		System.out.println("parameters received to convert pdf to jgp::'source':"+source+" ,'destination':"+destination+", 'format':"+format);
        boolean conversionFlag = false;
		try {
	        File sourceFile = new File(source);
	        if (sourceFile.exists()) {
	        	developerLog.debug("Source file exists");
	            PDDocument document = PDDocument.load(sourceFile);
	            List<PDPage> list = document.getDocumentCatalog().getAllPages();
	            for (PDPage page : list) {
	                BufferedImage image = page.convertToImage();
	                File outputfile = new File(destination);
	                System.out.println("Image Created -> "+ outputfile.getName());
	                ImageIO.write(image, format, outputfile);
	                developerLog.debug("File conversion complete");
	            }
	            document.close();
	            conversionFlag = true;
	        } else {
	        	conversionFlag = false;
	        	developerLog.debug("source file does not exist");
	        }
	
	    } catch (Exception e) {
	    	developerLog.debug("exception in file conversion complete");
	        e.printStackTrace();
	    }
		return conversionFlag;
	}
	
	
	
	public static void main(String[] args){
		new ConvertPDFPagesToImages().convertPDFToImage("D:/datacom/docubuilder/graph/Europe - LUX and Multi Manager/Appendix_Small Cap Core retail composite.pdf", "D:/datacom/docubuilder/graph/Europe - LUX and Multi Manager/Appendix_Small Cap Core retail composite.jpg", "jpg");
	}
}
