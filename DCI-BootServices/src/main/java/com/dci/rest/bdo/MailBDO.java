package com.dci.rest.bdo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dci.rest.common.ComponentCommons;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.Locale;
import com.dci.rest.model.Mail;
import com.dci.rest.model.Response;
import com.dci.rest.model.Status;

@Component
@SuppressWarnings({ "rawtypes", "unchecked" })
public class MailBDO extends ComponentCommons {

	@Value("${MAIL_SERVER}")
	private String server;
	@Value("${MAIL_SENDER}")
	private String senderAddress;

	private Logger developerLog = Logger.getLogger(MailBDO.class);

	public Response<Object> sendMail(Mail mail, HttpServletRequest request) {

		MDC.put("category", "com.dci.rest.bdo.MailBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.MailBDO || Method Name : sendMail ||");
		Multipart multipart = new MimeMultipart(); // Create a Multipart
		BodyPart messageBodyPart; // Create the message part
		StringBuffer sentMailIds = new StringBuffer();
		List<String> mailList = new ArrayList<String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			Session session = Session.getDefaultInstance(getMailProperties(), null);
			MimeMessage msg = new MimeMessage(session);
			InternetAddress addressFrom = new InternetAddress(mail.getFrom());
			msg.setFrom(addressFrom);

			String recipient[] = mail.getRecipient().split(",");

			String recipientCC[] = mail.getRecipientCC().split(",");

			// String recipientBCC[] = mail.getRecipientBCC().split(",");

			if (recipient.length > 0) {
				InternetAddress[] addressto = new InternetAddress[recipient.length];

				for (int i = 0; i < recipient.length; i++) {
					addressto[i] = new InternetAddress((String) recipient[i]);
					mailList.add(recipient[i]);
				}
				if (recipient.length == 1) {
					msg.setRecipients(Message.RecipientType.TO, addressto);
				} else {
					msg.setRecipients(Message.RecipientType.BCC, addressto);
				}
			}
			if (recipientCC.length > 0) {
				InternetAddress[] addressCC = new InternetAddress[recipientCC.length];
				for (int i = 0; i < recipientCC.length; i++) {
					InternetAddress ccAddress = new InternetAddress((String) recipientCC[i]);
					addressCC[i] = ccAddress;
					mailList.add(recipient[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, addressCC);
			}

			if (mail.getContent() != null) {
				messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(mail.getContent(), "text/html; charset=utf-8");
				multipart.addBodyPart(messageBodyPart);
			}

			/*
			 * if (legalText) { messageBodyPart = new MimeBodyPart();
			 * //messageBodyPart.setContent(emailWarningText,
			 * "text/html;charset=\"iso-8859-1\"" );
			 * messageBodyPart.setContent(emailWarningText, "text/html; charset=utf-8" );
			 * multipart.addBodyPart(messageBodyPart); }
			 */
			// System.out.println("MAILS : "+sentMailIds);
			msg.setSubject(mail.getSubject(), "UTF-8");
			msg.setContent(multipart);
			msg.setSentDate(new Date());
			Transport.send(msg);
			developerLog.debug("Exiting from com.dci.rest.bdo.MailBDO || Method Name : sendMail()");
			return response(200, "SUCCESS", null, mailList);
		} catch (Exception e) {
			developerLog.error("Exiting from com.dci.rest.bdo.MailBDO ||MethodName : sendMail ||" + e, e);
			new DocubuilderException("Exception in com.dci.rest.bdo.MailBDO || sendMail()", e);
			return response(400, "ERROR", invalidCompId, "MAIL SENDING FAILED");
		}
	}

	public Properties getMailProperties() {
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.host", server);
		properties.setProperty("mail.smtp.dsn.notify", "SUCCESS,FAILURE ORCPT=rfc822;" + senderAddress);
		properties.setProperty("mail.smtp.dsn.ret", "HDRS");
		return properties;
	}

}
