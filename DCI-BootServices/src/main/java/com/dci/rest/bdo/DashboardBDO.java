package com.dci.rest.bdo;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dci.rest.bean.DashboardLayout;
import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.DashboardDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.Response;
import com.dci.rest.model.Status;
import com.dci.rest.utils.CheckString;
@Component
public class DashboardBDO extends ComponentCommons{
	
	@Autowired DashboardDAO dashboardDAO;
	
	private Logger developerLog = Logger.getLogger(DashboardBDO.class);

	public Response<Object> getUserChartPrefrence(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.DashboardBDO");
	    developerLog.debug("Entering into com.dci.rest.bdo.DashboardBDO || Method Name : getUserChartPrefrence() ||");                
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		DashboardLayout dashboardLayout;
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }

			String userName = pathVariablesMap.get("user").toString();
	
			if(!CheckString.isValidString(pathVariablesMap.get("user"))) {
				status.setStatus("ERROR");
				status.setStatusCode(400);
				status.setMessage("required fields in path param");
				status.setRequire(new StringBuffer("user"));
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(null);
				return responseVOTemp;
			}
			 	dashboardLayout = dashboardDAO.getUserChartPrefrence(userName);
			 	return response(200,"SUCCESS",null,dashboardLayout);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.DashboardBDO || getUserChartPrefrence()", e);
			return response(500,"ERROR",internalServerError,pathVariablesMap);
		}
	}

	public Response<Object> updateChartPrefrence(Map<String, String> pathVariablesMap, DashboardLayout dashboardLayout,
			HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.DashboardBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.DashboardBDO || Method Name : updateChartPrefrence() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		if (!validateClientDetails(request)) {
			return validClientDetailsError();
		}
		String userName = pathVariablesMap.get("user").toString();
		try {
			if (!CheckString.isValidString(userName)) {
				status.setStatus("ERROR");
				status.setStatusCode(400);
				status.setMessage("required fields in path param");
				status.setRequire(new StringBuffer("user,client"));
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(null);
				return responseVOTemp;
			}
			String statusData = dashboardDAO.updateChartPrefrence(userName, dashboardLayout);
			return response(200, "SUCCESS", "", statusData);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.DashboardBDO || updateChartPrefrence()", e);
			return response(500, "ERROR", internalServerError, pathVariablesMap);
		}
	}
}
