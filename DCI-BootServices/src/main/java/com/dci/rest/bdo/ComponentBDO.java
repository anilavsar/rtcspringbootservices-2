package com.dci.rest.bdo;

import java.io.File;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.xerces.dom.DocumentImpl;
import org.json.XML;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.validation.BindingResult;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.dci.rest.bean.TableDetails;
import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.ComponentDAO;
import com.dci.rest.dao.elastic.ComponentElasticDAO;
import com.dci.rest.dao.elastic.ComponentElasticDBDAO;
import com.dci.rest.dao.elastic.ElasticServerKafkaDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.AssetClass;
import com.dci.rest.model.CommentsBean;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ComponentPermissionBean;
import com.dci.rest.model.ComponentStatus;
import com.dci.rest.model.ContextBean;
import com.dci.rest.model.ContextSchema;
import com.dci.rest.model.DocumentBean;
import com.dci.rest.model.DocumentType;
import com.dci.rest.model.Filter;
import com.dci.rest.model.FootNoteAttributes;
import com.dci.rest.model.Fund;
import com.dci.rest.model.Locale;
import com.dci.rest.model.Order;
import com.dci.rest.model.PendingWork;
import com.dci.rest.model.Placement;
import com.dci.rest.model.ResloveVarBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.Schema;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.model.SelectBean;
import com.dci.rest.model.ShareClass;
import com.dci.rest.model.Status;
import com.dci.rest.model.Style;
import com.dci.rest.model.StyleBean;
import com.dci.rest.model.TableComponent;
import com.dci.rest.model.TableType;
import com.dci.rest.model.Tag;
import com.dci.rest.model.UserBean;
import com.dci.rest.model.VariableBean;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.ConvertPDFPagesToImages;
import com.dci.rest.utils.DBXmlManager;
import com.dci.rest.utils.StringUtility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


@Component
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ComponentBDO extends ComponentCommons {
	
	@Autowired
	ComponentDAO compDAO;
	@Autowired ComponentElasticDAO searchDAO;
	
	@Autowired ElasticServerKafkaDAO elasticServerKafkaDAO;

	@Value("${ADD_FAV_LIMIT}" ) private String favlimit;
	@Value("${TEXT}" ) private String text;
	@Value("${SUBHEAD}" ) private String subhead;
	@Value("${IMAGE}" ) private String image;
	@Value("${VARIABLE}" ) private String variable;
	@Value("${SYS_SCHEMA}" ) private String schema;

	
	@Autowired UserPrivilegeBDO userPrevilege;
	
	
	private Logger developerLog = Logger.getLogger(ComponentBDO.class);
	private static final String FOR_FUND_TYPE = "VEHICLEASSETCLASS";
	private static final int commentLength = 600;
	private static final int searchType = 1006;
	private static String keyContextId = null;
	private static  String topContextId = null;

	public Response<Object> getComponentPrivilege(HttpServletRequest request){
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getComponentPrivilege() ||");
		Map<String, ComponentPermissionBean> privilegeBean= null;
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			privilegeBean = userPrevilege.getComponentPrivilege(userName, clientId);			
			return response(200,"SUCCESS","",privilegeBean);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentPrivilege()",e);
			return response(500,"ERROR",internalServerError,null);
		}
	}

	public Response<Object> getFilterList(HttpServletRequest request) {		
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getFilterList() ||");
		List<Filter> filters = new ArrayList<Filter>();
		Filter filterObj = new Filter(); 
		Filter pendingObj = new Filter();
		PendingWork pendingWorkObj = new PendingWork();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String username = request.getParameter("user").toString().toLowerCase();
			String departmentName = request.getParameter("client").toString();
			filters = compDAO.getSystemFilterList(username, departmentName);
			for (Filter filter : filters) {
				if (filter.getId().equalsIgnoreCase("fundspecific")) {
					List fundList = compDAO.getContentList(username, departmentName, null);
					filter.setItems(fundList);
				}
/*				if (filter.getId().equalsIgnoreCase("elementcontext")) {
					List contextList = compDAO.getElementContextList(username, departmentName, null);
					filter.setItems(contextList);
				}*/
				
				if (filter.getId().equalsIgnoreCase("elementcontext")) {
					List contextList = compDAO.getContentCategoryForSystemFilters(username, departmentName, null);
					filter.setItems(contextList);
				}
				
				if (filter.getId().equalsIgnoreCase("bystatus")) {
					List statusList = compDAO.getCompStatusList();
					filter.setItems(statusList);
				}
			}

			filterObj = pendingWork(username, departmentName);
			filterObj.setItems(filterObj.getItems());
			pendingWorkObj.setItems(filterObj.getItems());
			pendingWorkObj.setId("pendingwork");
			pendingWorkObj.setName("Pending Work");
			pendingObj.setPendingwork(pendingWorkObj);
			filters.add(pendingObj);
			return response(200,"SUCCESS",validName,filters);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getFilterList()",e);
			return response(500,"ERROR",internalServerError,filters);
		} 
		
	}
	private String queryBuilder(String resultCount) {
		String queryString = "{\"id\": \"dashboardtemplate\",\"params\": {\"size\":\""+resultCount+"\",\"pendingwork\":\"pendingwork\"}}";
		return queryString;
	}
	private Filter pendingWork(String userName,String deptNo){
		String pendingCount;
		List<Object> fundList = new ArrayList<Object>();
		Filter filter = new Filter();
		Set<String> fundSet = new HashSet<String>();
		List<ComponentBean>  finalkeyDistractorVarList = new ArrayList<ComponentBean>();
		List<ComponentBean>  finaltoContributorsList = new ArrayList<ComponentBean>();
		Object obj = null;JSONParser parser = new JSONParser();
		Map<String, String> appasenseResult;
		try {
		String queryDslCont = queryBuilder("0");
		//String queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"size\":\"0\",\"pendingwork\":\"pendingwork\"}}";
		Map<String, String> appasenseTotalCount = searchDAO.getPendingWork(queryDslCont,deptNo);
		
		obj = parser.parse(new StringReader(appasenseTotalCount.get("success")));
		org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
		org.json.simple.JSONObject resultObject = ((org.json.simple.JSONObject) jsonObject.get("hits"));
		pendingCount = resultObject.get("total").toString();
		String queryDslFinal = queryBuilder(pendingCount);
		//String finalqueryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"size\":\""+pendingCount+"\",\"pendingwork\":\"pendingwork\"}}";
		appasenseResult = new HashMap<String, String>();
		appasenseResult = searchDAO.getPendingWork(queryDslFinal,deptNo);
		List<ComponentBean> pendingWorkList = processESResultData(appasenseResult);
			for (ComponentBean componentBean : pendingWorkList) {
				String fundId[] = componentBean.getFundsAssociationId().split(";");
				String fundName[] = componentBean.getFundsAssociation().split(";");
				if (fundId.length > 0) {
					for (int i = 0; i < fundId.length; i++) {
						String funds = fundId[i].trim();
						if (!fundSet.contains(fundId[i].trim())) {
							Fund fundObj = new Fund();
							if (componentBean.getContext().equals("Key Detractors")) {
								keyContextId = componentBean.getContextId();
							}
							if (componentBean.getContext().equals("Top Contributors")) {
								topContextId = componentBean.getContextId();
							}
							Map<String, List<ComponentBean>> variableMap = compDAO.getFundVariableList(userName, deptNo,"1", componentBean, fundId[i].trim());
							fundSet.add(fundId[i].trim());
							finalkeyDistractorVarList = variableMap.get("Key_Detractor");
							finaltoContributorsList = variableMap.get("Top_Contributor");
							if (finalkeyDistractorVarList.isEmpty() && finaltoContributorsList.isEmpty()) {
								continue;
							}
							List<Tag> categoryList = new ArrayList<Tag>();
							fundObj.setfId(funds);
							fundObj.setName(fundName[i]);

							if (!finalkeyDistractorVarList.isEmpty()) {
								Tag variables = new Tag();
								Style variableValues;
								variables.setName("Key Detractors");
								if (CheckString.isValidString(keyContextId)) {
									variables.setId(Integer.parseInt(keyContextId));
								}
								List<Style> variableValuesList = new ArrayList<Style>();
								for (ComponentBean componentBean2 : finalkeyDistractorVarList) {
									variableValues = new Style();
									if (CheckString.isValidString(componentBean2.getVariableValue())) {
										variableValues.setValue(componentBean2.getVariableValue());
										variableValuesList.add(variableValues);
									}

								}
								variables.setCategory(variableValuesList);
								categoryList.add(variables);
								fundObj.setCategory(categoryList);
							}
							if (!finaltoContributorsList.isEmpty()) {
								Tag category2 = new Tag();
								List<Tag> categoryList2 = new ArrayList<Tag>();
								Style variableValues;
								category2.setName("Top Contributors");
								if (CheckString.isValidString(topContextId)) {
									category2.setId(Integer.parseInt(topContextId));
								}
								List<Style> variableValuesList1 = new ArrayList<Style>();
								for (ComponentBean componentBean2 : finaltoContributorsList) {
									variableValues = new Style();
									if (CheckString.isValidString(componentBean2.getVariableValue())) {
										variableValues.setValue(componentBean2.getVariableValue());
										variableValuesList1.add(variableValues);
									}
								}
								category2.setCategory(variableValuesList1);
								categoryList2.add(category2);
								for (Tag style : categoryList2) {
									categoryList.add(style);
								}
								fundObj.setCategory(categoryList);
							}
							if (fundObj != null)
								fundList.add(fundObj);
						}
					}
				}
				// keyContextId =null;
				// topContextId =null;
			}
			 filter.setItems(fundList);
		}catch (Exception e) {
			new DocubuilderException("Exception in Entering into com.dci.rest.bdo.ComponentBDO || processESResultData()", e);
		}
		return filter;
	}	
	private List<ComponentBean> processESResultData(Map<String, String> result) {
		Object obj = null;
		List<ComponentBean> pendingWorkList = new ArrayList<ComponentBean>();
		try {
			JSONParser parser = new JSONParser();
			obj = parser.parse(new StringReader(result.get("success")));
			ComponentBean componentBean,componentBean2;
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			org.json.simple.JSONArray resultObject = (org.json.simple.JSONArray) ((org.json.simple.JSONObject) jsonObject.get("hits")).get("hits");
			for (int idx = 0; idx < resultObject.size(); idx++) {
				componentBean = new ComponentBean();
				org.json.simple.JSONObject componentObject = (org.json.simple.JSONObject) resultObject.get(idx);
				componentBean.setElementInstanceId(componentObject.get("_id").toString());
				componentObject = (org.json.simple.JSONObject) componentObject.get("_source");
				componentBean.setContext(componentObject.get("felementcontext_desc").toString());
				componentBean.setContextId(componentObject.get("felementcontextid").toString());
				componentBean.setFundsAssociation(componentObject.get("fund_association").toString());
				componentBean.setFundsAssociationId(componentObject.get("fund_association_id").toString());
				pendingWorkList.add(componentBean);
			}
/*			componentBean2 = new ComponentBean();
			componentBean2.setContext("Key Detractors");
			componentBean2.setContextId("57");
			componentBean2.setFundsAssociation("Fund055_12");
			componentBean2.setFundsAssociationId("4");
			pendingWorkList.add(componentBean2);

			componentBean2 = new ComponentBean();
			componentBean2.setContext("Top Contributors");
			componentBean2.setContextId("57");
			componentBean2.setFundsAssociation("ActiveBeta EAFE International Equity ETF");
			componentBean2.setFundsAssociationId("469");
			pendingWorkList.add(componentBean2);
*/
			return pendingWorkList;
		} catch (Exception e) {
			new DocubuilderException("Exception in Entering into com.dci.rest.bdo.ComponentBDO || processESResultData()", e);
		}
		return pendingWorkList;
	}	
	public Response<Object> isValidName(String name, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : isValidName() ||");
		String isValidName = "";
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			isValidName = compDAO.checkComponentName(user, client, null, name);
			if (isValidName != null && !isValidName.trim().equalsIgnoreCase("[]")) {
				return response(409, "ERROR", duplicate, name);
			} else {
				return response(200, "SUCCESS", validName, name);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || isValidName()", e);
			return response(500, "ERROR", internalServerError, name);
		}
	}

	public Response<Object> createComponent(ComponentBean componentObj, Map<String,Object> reqParams,BindingResult bindingResult,HttpServletRequest request) throws Exception{
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createComponent() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
	    if(!validateClientDetails(request)) {
	    	return validClientDetailsError();
	    }  
		String userName = request.getParameter("user").toString();
		String clientId = request.getParameter("client").toString();
	    if (bindingResult.hasErrors()) {
			return validateRequiredField(bindingResult,componentObj);
		}
		componentObj.setOperation("create");
		if(! userPrevilege.checkComponentPerrmission(userName, clientId,componentObj )) {
			return response(403,"ERROR",insuff_previlage,componentObj);
		}
		String bookInstanceId = reqParams.get("bookInstanceId") != null ? (String) reqParams.get("bookInstanceId") : null;
		String bookDetailId = reqParams.get("bookDetailId") != null ? (String) reqParams.get("bookDetailId") : null;
		developerLog.debug("body : "+componentObj.getBody());
		componentObj.setBody( processComponentBody(componentObj, reqParams,request,false) );
		componentObj.setName(StringUtility.replaceControlCharacters(componentObj.getName(),null));
		componentObj.setGlobal("1");
		componentObj.setQualDataCon("default");		
		componentObj.setFootnoteIds(getHiddenValues(componentObj.getBody(), "footnote"));
		componentObj.setRecursiveIds(getHiddenValues(componentObj.getBody(), "embedded"));
		componentObj.setEntityIds(getHiddenValues(componentObj.getBody(), "userVariable"));
		componentObj.setSystemEntityIds(getHiddenValues(componentObj.getBody(), "systemVariable"));
    	if(componentObj.getType().equals("footnote"))
    	{
    		if(CheckString.isValidString(componentObj.getFootnoteIds()) && !componentObj.getFootnoteIds().equals("0")) {
    	   		int statusCode = compDAO.checkForDuplicateFootnoteId(userName, clientId,componentObj.getFootnoteMapping());
    		
     		if(statusCode == -1) {
    			return response(409,"ERROR",duplicateFtId,componentObj);
    		}
    		}
    	}
		//if(new ComponentSearchBDO().isCompCreateEditPermission(request, componentObj.getType(), "create")){
			String existingComponentResult = compDAO.checkComponentName(userName, clientId, null, componentObj.getName());
	        if(existingComponentResult != null && !existingComponentResult.trim().equalsIgnoreCase("[]")){
        	     return response(409,"ERROR",duplicate,componentObj);
	        }else if(existingComponentResult != null && existingComponentResult.trim().equalsIgnoreCase("[]")){
	        	Map<String, String> objectNode = createComponent(userName, clientId, bookInstanceId, bookDetailId, componentObj);
	        	setComponentAssociation(userName, clientId, componentObj);
	        	compDAO.setComponentAssignee(userName,clientId,objectNode.get("elementId"),componentObj);
	        	if(componentObj.getTagAssociation()!=null && componentObj.getTagAssociation().size()>0) {
	        		Map<String, String> tempParam = new HashMap<String, String>();
	        		tempParam.put("user", userName);
	        		tempParam.put("client", clientId);
	        		tempParam.put("componentId", componentObj.getElementInstanceId());
	        		createTags(tempParam, componentObj,request);
	        	}
	        	if(objectNode.get("message").toString().equals("success")) {
	        		componentObj.setElementInstanceId(objectNode.get("elementId"));
	        		//update component Kafka Service
//	        		elasticServerKafkaDAO.kafkaProducerNotifyComponentUpdation(userName, clientId, componentObj.getElementInstanceId());
	        		return response(200,"SUCCESS","",componentObj);
	        	}else if(objectNode.get("message").toString().equals("texttoolong")){
	        		return response(413,"ERROR",textTooLong,componentObj);
	        	}else if(objectNode.get("message").toString().equals("footnoteid already exist,error")){
	        		return response(409,"ERROR",duplicateFtId,componentObj);
	        	}
	        	else
	        	{
	        		return response(500,"ERROR",internalServerError,componentObj);
	        	}
	            	//setTagAssociation(componentObj, requiredSessionAttributes);
	        }
		//}
			return responseVOTemp;
	}
	public Map<String, String> createComponent(String user, String clientId, String bookInstanceId, String bookDetailId, ComponentBean componentOBj) throws Exception {
		return compDAO.createComponent(user, clientId, bookInstanceId, bookDetailId, componentOBj);
	}
	
	public Response<Object> getContextList(HttpServletRequest request){
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		List<ContextBean> contextList = new ArrayList<ContextBean>();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			contextList = compDAO.getElementContextList(userName, clientId,null);
			return response(200,"SUCCESS","",contextList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getContextList()",e);
			return response(500,"ERROR",internalServerError,contextList);
		}
	}
/*public Response<Object> getContextListTemp(HttpServletRequest request) {
	MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
	developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getContentCategory() ||");
	List<ContextBean> contextBeansList = new ArrayList<ContextBean>();
	List<ContextBean> contextBeansListFinal = new ArrayList<ContextBean>();
	Map<Integer, String> categoryMap = new HashMap<Integer, String>();
	ContextBean contextBeanObj;
	try {
		if (!validateClientDetails(request)) {
			return validClientDetailsError();
		}
		String user = request.getParameter("user").toString();
		String client = request.getParameter("client").toString();

			contextBeansList = compDAO.getContentCategory(user, client, null);
			for (ContextBean contextBean : contextBeansList) {
				categoryMap.put(contextBean.getId(), contextBean.getName());
			}
			for (Map.Entry<Integer, String> entry : categoryMap.entrySet()) {
				contextBeanObj = new ContextBean();
				contextBeanObj.setId(entry.getKey());
				contextBeanObj.setName(entry.getValue());
				contextBeansListFinal.add(contextBeanObj);
			}
			for (ContextBean contextBeanTemp : contextBeansList) {
				contextBeanObj = new ContextBean();
				contextBeanObj.setId(Integer.parseInt(contextBeanTemp.getsId()));
				contextBeanObj.setName(contextBeanTemp.getSname());
				contextBeanObj.setpId(contextBeanTemp.getId());
				contextBeansListFinal.add(contextBeanObj);
			}
				return response(200, "SUCCESS", "", contextBeansListFinal);
		} catch (Exception e) {
		new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getContentCategory()", e);
		return response(500, "ERROR", internalServerError, null);
	}
}*/
	public Response<Object> getLocalList(HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		List<Locale> localList = new ArrayList<Locale>();
		try {
		    if(!validateClientDetails(request)) {
					return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			localList = compDAO.getLanguageList(clientId);
			return response(200,"SUCCESS","",localList);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getLocalList()",e);
			return response(500,"ERROR",internalServerError,localList);
		}
	}

	public Response<Object> getComponent(String componentId, Map<String, Object> reqParams,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		ComponentBean component = new ComponentBean();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			if (CheckString.isNumericValue(componentId)) {
				component = compDAO.getComponent(userName, clientId, componentId);
				component.setOldFootnoteIds(getHiddenValues(component.getBody(), "footnote"));
				component.setFootnoteIds(getHiddenValues(component.getBody(), "footnote"));

				component.setRecursiveIds(getHiddenValues(component.getBody(), "embedded"));
				component.setOldRecursiveIds(getHiddenValues(component.getBody(), "embedded"));

				component.setEntityIds(getHiddenValues(component.getBody(), "userVariable"));
				component.setOldEntityIds(getHiddenValues(component.getBody(), "userVariable"));

				component.setSystemEntityIds(getHiddenValues(component.getBody(), "systemVariable"));
				component.setOldSystemEntityIds(getHiddenValues(component.getBody(), "systemVariable"));
		 
				component.setBody(getConvertedBody(component.getBody(), component.getType()));
				return response(200,"SUCCESS","",component);
			} else {
				return response(400,"ERROR",invalidCompId,componentId);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponent()",e);
			return response(500,"ERROR",internalServerError,componentId);
		}
	}

	public Response<Object> editComponent(String idToEdit, ComponentBean componentObj, Map<String, Object> reqParams,HttpServletRequest request){
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		Map<String, Object> updateProcessStatus = new HashMap<String, Object>();
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			//reqParams.put("request", request);
			componentObj.setOperation("edit");
			if(CheckString.isValidString(componentObj.getType()) && ! userPrevilege.checkComponentPerrmission(userName, clientId,componentObj )) {
				return response(403,"ERROR",insuff_previlage,componentObj);
			}
			componentObj.setElementInstanceId(idToEdit);
			ComponentBean compDataFromDB =  compDAO.getComponentData(componentObj.getElementInstanceId());

			if(!CheckString.isValidString(componentObj.getType())) {
				componentObj.setType(compDataFromDB.getType());
			}
			//check if name is already exist
			if(CheckString.isValidString(componentObj.getName()) && CheckString.isValidString(componentObj.getElementInstanceId())) {
				String existingComponentResult = compDAO.checkComponentName(userName, clientId, componentObj.getElementInstanceId(), componentObj.getName());
				if(existingComponentResult != null && !existingComponentResult.trim().equalsIgnoreCase("[]")){
					return response(409,"ERROR",duplicate,componentObj);
				}				 
	        }
			if(CheckString.isNumericValue(componentObj.getElementInstanceId())){
				boolean isFieldUpdate = false;//this flag can be used for future reference where the legacy code has written to update the component inside the if block dao call
	    		if(CheckString.isValidString(componentObj.getName()) || CheckString.isValidString(componentObj.getBody()) || CheckString.isValidString(componentObj.getBody())|| CheckString.isValidString(componentObj.getLocale())|| CheckString.isValidString(componentObj.getContext())|| CheckString.isValidString(componentObj.getEffectiveDate())|| CheckString.isValidString(componentObj.getExpirationDate())||CheckString.isEmptyString(componentObj.getEffectiveDate()) ||  CheckString.isEmptyString(componentObj.getExpirationDate())||CheckString.isValidString(componentObj.getFcomp_assignee()) ||CheckString.isValidString(componentObj.getWorkFlowStatusId()) ||CheckString.isValidString(componentObj.getOwner())) {
	    			isFieldUpdate = true;
    			}
	    		if(isFieldUpdate) {	
					//ComponentBean compDataFromDB =  compDAO.getComponentData(componentObj.getElementInstanceId());
					componentObj.setQualData_Id(compDataFromDB.getQualData_Id());
					componentObj.setQualDataId(compDataFromDB.getQualDataId());
					componentObj.setLastUpdatedTime(compDataFromDB.getLastUpdatedTime());
					
					String bookInstanceId = reqParams.get("bookInstanceId") != null ? (String) reqParams.get("bookInstanceId") : "";
					String bookDetailId = reqParams.get("bookDetailId") != null ? (String) reqParams.get("bookDetailId") : "";
					String contentChangedFlag = "false";				
					//convert component type  process
					if((CheckString.isValidString(componentObj.getConvertTo()) && !componentObj.getConvertTo().equalsIgnoreCase("-1")) && (componentObj.getType().equalsIgnoreCase("para")||componentObj.getType().equalsIgnoreCase("bridgehead"))){
						compDAO.convertComponent(userName, clientId, componentObj.getElementInstanceId(), compDataFromDB.getType(), componentObj.getConvertTo());
					}
					if(CheckString.isValidString(componentObj.getBody())) {
						componentObj.setBody(processComponentBody(componentObj, reqParams,request,true) );
						componentObj.setFootnoteIds(getHiddenValues(componentObj.getBody(), "footnote"));
						componentObj.setRecursiveIds(getHiddenValues(componentObj.getBody(), "embedded"));
						componentObj.setEntityIds(getHiddenValues(componentObj.getBody(), "userVariable"));
						componentObj.setSystemEntityIds(getHiddenValues(componentObj.getBody(), "systemVariable"));
					}		
					componentObj.setGlobal("1");
					componentObj.setQualDataCon("default");	
					if(CheckString.isValidString(componentObj.getWorkFlowStatusId())) {
						WorkFLStatus workFLStatus = compDAO.getStatusDefaultAssignee(userName,clientId,componentObj.getWorkFlowStatusId(),schema);
						if(CheckString.isValidString(workFLStatus.getAssignee()) && !CheckString.isValidString(componentObj.getFcomp_assignee())) {
							componentObj.setFcomp_assignee(workFLStatus.getAssignee());
						}						
	    		     } 
                    if(CheckString.isValidString(componentObj.getLocale())){
                    	compDAO.setComponentLanguage(userName, componentObj.getElementInstanceId(), componentObj.getLocale());
                    }
		        	Map<String, Object> paramMap = new HashMap<String, Object>();
		        	paramMap.put("componentObj",componentObj);
		    		paramMap.put("overwriteStatus",(CheckString.isValidString(componentObj.getOverrideStatus())&&componentObj.getOverrideStatus().equalsIgnoreCase("y")) ? "Y" : "N");
		    		paramMap.put("bookInstanceId",bookInstanceId);
		    		paramMap.put("bookDetailId",bookDetailId); 
		    		paramMap.put("qualDataCond",(CheckString.isValidString(componentObj.getQualDataCon())) ? componentObj.getQualDataCon() : "");
		    		paramMap.put("qualDataTableType",(CheckString.isValidString(componentObj.getQualDataTableType())) ? componentObj.getQualDataTableType() : "");
		    		if(CheckString.isValidString(componentObj.getBody()) && !compDataFromDB.getBody().equals(componentObj.getBody())){
		    			contentChangedFlag = "true";
		    		}
		    		paramMap.put("contentchanged",contentChangedFlag);	    			    		
		    		//paramMap.put("lastUpdatedTime",(request.getParameter("lastUpdatedTime")!=null) ? request.getParameter("lastUpdatedTime") : "");
		        	
		    		//update component data    		    			
	    			updateProcessStatus.put("updateStatus", compDAO.editComponentField(userName, clientId, paramMap));
	    			//updateProcessStatus.put("updateStatus", compDAO.editComponentData(userName, clientId, paramMap));
	    		}else {  			
	    			//updateProcessStatus.put("updateStatus", compDAO.editComponentData(userName, clientId, paramMap));for legacy code update	
	    			//set Association process
		    		if(CheckString.isValidString(componentObj.getDocumentAssociation()) || CheckString.isValidString(componentObj.getDocumentTypeAssociation()) || CheckString.isValidString(componentObj.getFundsAssociation()) || CheckString.isValidString(componentObj.getAssetClassAssociation())) {
		    			updateProcessStatus.put("associationStatus",setComponentAssociation(userName, clientId, componentObj));
		    		}	    		
		        	//set tag association
		    		if(CheckString.isValidString(componentObj.getTags())){
		    			updateProcessStatus.put("tagStatus",setTagAssociation(userName, clientId, componentObj));
		    		}
		    		//update footnote mapping
		    		if(componentObj.getType().equalsIgnoreCase("footnote")&& !componentObj.getFootnoteMapping().equals(componentObj.getPreviousFootnoteMapping())){
		    			updateProcessStatus.put("footnoteMappingStatus",processFootnoteMapping(userName, clientId, componentObj));
		    		}
	    		}	
	    		//update component Kafka Service
//        		elasticServerKafkaDAO.kafkaProducerNotifyComponentUpdation(userName, clientId, idToEdit);
	    		return response(200,"SUCCESS","",updateProcessStatus);
			}else {
				return response(400,"ERROR",invalidCompId,idToEdit);
			}
		}catch (Exception e) { 
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editComponent()",e);
		}		
        return responseVOTemp;
	}
	@SuppressWarnings("unused")
	public Response<Object> setComponentAssociation(String componentId,ComponentBean component, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();		
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if(CheckString.isNumericValue(componentId)) {
				component.setElementInstanceId(componentId);
				component.setOperation("edit");
				objectNode = setComponentAssociation( userName,clientId,component);		
				return response(200,"SUCCESS","",componentId);

			}else {
				return response(400,"ERROR",invalidCompId,componentId);
			}
		}catch(Exception  e){	
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || setComponentAssociation()",e);
			return response(500,"ERROR",internalServerError,componentId);
        } 
	}
	
	public ObjectNode setComponentAssociation(String user, String clientId,  ComponentBean component) throws Exception {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		if(!CheckString.isValidString(component.getType())) {//to get type of the component
			ComponentBean compDataFromDB =  compDAO.getComponentData(component.getElementInstanceId());
			component.setType(compDataFromDB.getType());
		}
		//if(CheckString.isValidString(component.getDocumentAssociation()) || CheckString.isValidString(component.getDocumentTypeAssociation()) || CheckString.isValidString(component.getFundsAssociation()) || CheckString.isValidString(component.getAssetClassAssociation())) {
			if(component.getOperation().equalsIgnoreCase("edit") && component.getElementInstanceId()!=null){
				compDAO.dissociateComponent(user, clientId, component.getElementInstanceId());
			}
	//	}		
		if(CheckString.isValidString(component.getDocumentAssociation())) {
			objectNode.put("documentAssociationStatus", compDAO.setDocumentAssociation(user, component.getElementInstanceId(), component.getDocumentAssociation()));
		}
		if(CheckString.isValidString(component.getDocumentTypeAssociation())) {
			objectNode.put("docTypeAssociationStatus", compDAO.setDocTypeAssociation(user, component.getElementInstanceId(), component.getDocumentTypeAssociation()));
		}
		if(CheckString.isValidString(component.getAssetClassAssociation())) {
			objectNode.put("assetClassAssociationStatus", compDAO.setAssetClassAssociation(user, component.getElementInstanceId(), component.getAssetClassAssociation(), component.getType()));
		}
		if(CheckString.isValidString(component.getFundsAssociation())) {
			objectNode.put("fundAssociationStatus", compDAO.setFundAssociation(user, component.getElementInstanceId(), component.getFundsAssociation()));
		}
		return objectNode;
	}
	
	public String setTagAssociation(String userName, String clientId, ComponentBean compObj) throws Exception{
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		Map<String, Object> tagAsstnStatus = new HashMap<String, Object>();
		if(CheckString.isValidString(compObj.getElementInstanceId())){
			tagAsstnStatus = compDAO.setTagAssociation(userName, Integer.parseInt(clientId), compObj.getElementInstanceId(), compObj.getTags());
		}
		return new ObjectMapper().writeValueAsString(tagAsstnStatus);
	}
	public String processFootnoteMapping(String userName, String clientId, ComponentBean compObj) throws Exception{
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode(); 
		objectNode.put("disassociateMapping", compDAO.dissociateFootnoteMapping(userName, clientId, compObj.getElementInstanceId(), compObj.getPreviousFootnoteMapping()));
		objectNode.put("associateMapping", compDAO.checkFootnoteMappingExistence(userName, clientId, compObj.getElementInstanceId(), compObj.getFootnoteMapping()));
		return objectNode.toString();
	}
	
	public Response<Object> getAssociationData(HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		Map<String, Object> assocationData = new HashMap<String, Object>();
		try {
			assocationData.put("documentAssociationList", getDocumentList(request).getResult());
			assocationData.put("docTypeAssociationList", getDocTypeList(request).getResult());
			assocationData.put("fundAssociationList", getFundList(request).getResult());
			assocationData.put("assetClassList", getAssetClassList(request).getResult());
			return response(200,"SUCCESS","",assocationData);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getAssociationData()",e);
			return response(500,"ERROR",internalServerError,assocationData);
		}
	}
	
	public Response<Object> getComponentAssociation(String componentId, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		Map<String, Object> assocationData = new HashMap<String, Object>();
		ArrayList<Fund> fundsArray = new ArrayList<Fund>();
		ArrayList<Fund> fundsArrayFinal = new ArrayList<Fund>();
		List<Fund> found = new ArrayList<Fund>();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if(CheckString.isValidString(componentId)) {
				assocationData.put("documentAssociationList", compDAO.getDocumentList(userName, clientId, componentId));
				assocationData.put("docTypeAssociationList", compDAO.getDcoTypeList(userName,clientId, componentId));
				List<AssetClass> assetClassList = compDAO.getAssetClassList(userName, clientId, componentId);
				assocationData.put("assetClassList",assetClassList);
				fundsArray = compDAO.getContentList(userName, clientId, componentId);
				List<Fund> fundList = null ;
				for (int i = 0;i<assetClassList.size();i++) {
					fundList = new ArrayList<Fund>();
					fundList = compDAO.getFundListOnAssetClass(userName,clientId,assetClassList.get(i).getId());
					for (int j = 0;j<fundList.size();j++) {
						fundList.get(j).setAssetClassId(assetClassList.get(i).getId());
						fundList.get(j).setAssetClassName(assetClassList.get(i).getName());
						fundsArrayFinal.add(fundList.get(j));
					}
				}
				for (Fund fundArr : fundsArray) {
					if (fundList != null) {
						for (Fund fundLi : fundList) {
							if (fundArr.getId() == fundLi.getId()) {
								found.add(fundArr);
							}
						}
					}
				}
				fundsArray.removeAll(found);
				if (fundsArrayFinal != null) {
					fundsArray.addAll(fundsArrayFinal);
				}
				assocationData.put("fundAssociationList", fundsArray);
				return response(200,"SUCCESS","",assocationData);
			}else {
				return response(400,"ERROR",invalidCompId,componentId);
			}
		}catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentAssociation()",e);
			return response(500,"ERROR",internalServerError,componentId);
		}
	}
	
	public Response<Object> getDocumentList(HttpServletRequest request) throws Exception {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		ArrayList<DocumentBean> documentsArray = new ArrayList<DocumentBean>();
		if (!validateClientDetails(request)) {
			return validClientDetailsError();
		}
		try {
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			documentsArray = compDAO.getDocumentList(userName, clientId, null);
			return response(200, "SUCCESS", "", documentsArray);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getDocumentList()", e);
			return response(500, "ERROR", internalServerError, documentsArray);
		}
	}
	
	public Response<Object> getDocTypeList(HttpServletRequest request) throws Exception {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		if (!validateClientDetails(request)) {
			return validClientDetailsError();
		}
		try {
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			ArrayList<DocumentType> documentTypeArray = new ArrayList<DocumentType>();
			documentTypeArray = compDAO.getBookTypeList(userName, clientId);
			return response(200, "SUCCESS", "", documentTypeArray);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getDocTypeList()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}

	public Response<Object> getCreatedUpdatedBy(HttpServletRequest request) throws Exception {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		ArrayList<UserBean> userlist = new ArrayList<UserBean>();

		if (!validateClientDetails(request)) {
			return validClientDetailsError();
		}
		String userName = request.getParameter("user").toString();
		String clientId = request.getParameter("client").toString();
		try {
			userlist = compDAO.getCreatedUpdatedBy(userName, clientId);
			return response(200, "SUCCESS", "", userlist);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getCreatedUpdatedBy()", e);
			return response(500, "ERROR", internalServerError, null);
		}

	}

	public Response<Object> getStatusList(HttpServletRequest request) throws Exception {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			List<ComponentStatus> statusList = new ArrayList<ComponentStatus>();
			statusList = compDAO.getCompStatusList();
			return response(200, "SUCCESS", "", statusList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getStatusList()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	
	public Response<Object> getFundList(HttpServletRequest request) throws Exception {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		ArrayList<Fund> fundsArray = new ArrayList<Fund>();
		if (!validateClientDetails(request)) {
			return validClientDetailsError();
		}
		String userName = request.getParameter("user").toString();
		String clientId = request.getParameter("client").toString();
		try {
			fundsArray = compDAO.getContentList(userName, clientId, null);
			return response(200, "SUCCESS", "", fundsArray);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getFundList()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	
	public Response<Object> getAssetClassList(HttpServletRequest request) throws Exception {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		ArrayList<AssetClass> assetClassArray = new ArrayList<AssetClass>();
		if (!validateClientDetails(request)) {
			return validClientDetailsError();
		}
		String userName = request.getParameter("user").toString();
		String clientId = request.getParameter("client").toString();
		try {
			assetClassArray = compDAO.getContentCategoryList(userName, clientId, FOR_FUND_TYPE);
			return response(200, "SUCCESS", "", assetClassArray);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getAssetClassList()", e);
			return response(500, "ERROR", internalServerError, null);
		}

	}
	
	public Response<Object> getComponentComments(String componentId, HttpServletRequest request) throws JsonProcessingException {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
	    developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getComponentComments() ||");                
       try{			
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (CheckString.isValidString(componentId)) {
				List<CommentsBean> commentList = compDAO.getAllCommentsOfComponent(userName,clientId,componentId);
				return response(200, "SUCCESS", "", commentList);
			} else {
				return response(400, "ERROR", invalidCompId, componentId);
			}

        }catch(Exception e){
        	new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentComments()",e);
        	return response(500,"ERROR",internalServerError,componentId);
        }
	}
	public Response<Object> getLinkageComponent(String componentId, HttpServletRequest request) throws JsonProcessingException {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
	    developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getLinkageComponent() ||");                
	 	List<ComponentBean> secondaryComponents = new ArrayList<ComponentBean>();	
	     try{
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			if (CheckString.isValidString(componentId)) {
				secondaryComponents = compDAO.getSecondaryComponentsForPrimaryComponent(clientId, componentId);
				return response(200,"SUCCESS","",secondaryComponents);
			} else {
				return response(400,"ERROR",invalidCompId,componentId);
		}
       	}catch(Exception e){
           	new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getLinkageComponent()",e);
           	return response(500,"ERROR",internalServerError,componentId);
       }
	}

	public Response<Object> processComponentPrimaryFlag(String componentId, String flag, HttpServletRequest request)
			throws Exception {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : processComponentPrimaryFlag() ||");
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (CheckString.isValidString(flag) && (flag.equalsIgnoreCase("true") || flag.equalsIgnoreCase("false"))) {
				if (CheckString.isNumericValue(componentId)) {
					String primaryFlag = flag;
					if (compDAO.getSecondaryComponentsForPrimaryComponent(clientId, componentId).size() == 0) {
						if (primaryFlag.equalsIgnoreCase("true")) {
							objectNode.put("setPrimaryStatus", compDAO.setPrimaryComponent(userName, componentId));
						} else if (primaryFlag.equalsIgnoreCase("false")) {
							objectNode.put("unSetPrimaryStatus", compDAO.unSetPrimaryComponent(userName, componentId));
						}
					} else {
						return response(400, "ERROR", notIndependent, null);
					}
				} else {
					return response(400, "ERROR", invalidCompId, null);
				}

			} else {
				return response(400, "ERROR", invalidPrimaryFlg, null);
			}
			return response(200, "SUCCESS", "", objectNode);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || processComponentPrimaryFlag()", e);
			return response(500, "ERROR", internalServerError, componentId);
		}
	}
	
	public Response<Object> createComponentComment(String componentId, ComponentBean component,
			HttpServletRequest request, BindingResult bindingResult) throws JsonProcessingException {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createComponentComment() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (CheckString.isNumericValue(componentId)) {
				if (bindingResult.hasErrors()) {
					return validateRequiredField(bindingResult, component);
				}
				if (CheckString.isValidString(component.getComment())) {
					String comment = StringUtility.fixSpecialCharforWeb(component.getComment());
					if (compDAO.insertCommentForElement(userName, componentId, comment)) {
						return response(200, "SUCCESS", "", component);
					} else {
						return response(500, "ERROR", internalServerError, component);
					}
				}
			} else {
				return response(400, "ERROR", invalidCompId, componentId);
			}
			return response(200, "SUCCESS", "", component);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createComponentComment()", e);
			return response(500, "ERROR", internalServerError, component);
		}
	}
	public Response<Object> getComponentHistoryData(String componentId,HttpServletRequest request) throws JsonProcessingException {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
	    developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getComponentHistoryData() ||");                
	    List<ComponentBean> componentHistoryList = new ArrayList<ComponentBean>();
       try{
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			if (CheckString.isValidString(componentId)) {
				componentHistoryList = compDAO.getRevisionHistoryForComponent(userName, componentId);
				for (ComponentBean componentHistoryObj : componentHistoryList) {
					String qualData = componentHistoryObj.getBody();
					qualData = changeFootnoteValues(componentHistoryObj.getBody());
					if (qualData != null && qualData.length() > commentLength) {
						qualData = convertTags(qualData);

						int trimLength = CheckString.getTrimLength(qualData, commentLength);
						qualData = qualData.trim().substring(0, trimLength);
						qualData = CheckString.formatHTMLString(qualData);
						componentHistoryObj.setBody(qualData);
					}
				}
				return response(200,"SUCCESS","",componentHistoryList);
			}else {
				return response(400,"ERROR",invalidCompId,componentId);
			}
       	}catch(Exception e){
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentHistoryData()", e);
			return response(500,"ERROR",internalServerError,componentId);
        }
	}

	public Response<Object> disassociateSecondaryComonent(String primaryComponentId, ComponentBean component,
			HttpServletRequest request) throws JsonProcessingException {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug(
				"Entering into com.dci.rest.bdo.ComponentBDO || Method Name : disassociateSecondaryComonent() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();

		boolean isDisassociated = false;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (CheckString.isNumericValue(primaryComponentId)) {
				boolean isPrimary = compDAO.isPrimaryComponent(primaryComponentId);
				String elementChildId[] = component.getElementInstanceId().split(",");
				if (elementChildId.length > 0) {
					for (int i = 0; i < elementChildId.length; i++) {
						if (isPrimary == true) {
							isDisassociated = compDAO.disassociateSecondaryComonent(userName, elementChildId[i],
									primaryComponentId);
						} else {
							isDisassociated = compDAO.disassociateSecondaryComonent(userName, primaryComponentId,
									elementChildId[i]);
						}
					}
					if (isDisassociated) {
						return response(200, "SUCCESS", "", "success");
					} else {
						return response(200, "SUCCESS", "", "failure");
					}
				} else {
					status.setStatusCode(400);
					status.setStatus("ERROR");
					status.setMessage("Requied field");
					status.setRequire(new StringBuffer("elementInstanceId"));
					responseVOTemp.setStatus(status);
					responseVOTemp.setResult(null);

				}
			}
			return response(400, "ERROR", invalidCompId, null);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || disassociateSecondaryComonent()",
					e);
			return response(500, "ERROR", internalServerError, primaryComponentId);
		}
	}

	public Response<Object> linkComponents(String componentId, ComponentBean component, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : processComponentPrimaryFlag() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		int counter = 0;
		Map<String, String> result = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (CheckString.isNumericValue(componentId)) {
				if (!CheckString.isValidString(component.getElementInstanceId())) {
					status.setStatusCode(400);
					status.setStatus("ERROR");
					status.setMessage("Requied field");
					status.setRequire(new StringBuffer("elementInstanceId"));
					responseVOTemp.setStatus(status);
					responseVOTemp.setResult(null);
				}
				String elementChildId[] = component.getElementInstanceId().split(",");

				boolean isPrimary = compDAO.isPrimaryComponent(componentId);
				if (elementChildId.length > 0) {
					for (int i = 0; i < elementChildId.length; i++) {
						boolean isSuccess = false;
						if (isPrimary) {
							isSuccess = compDAO.setAssociationElementToElement(userName, clientId, elementChildId[i],
									componentId);
						} else {
							isSuccess = compDAO.setAssociationElementToElement(userName, clientId, componentId,
									elementChildId[i]);
						}
						if (isSuccess) {
							result.put(elementChildId[i], "linked Successfull");
						} else {
							++counter;
							result.put(elementChildId[i], "error to link");
						}
					}
				} else {
					status.setStatusCode(400);
					status.setStatus("ERROR");
					status.setMessage("Requied field");
					status.setRequire(new StringBuffer("elementInstanceId"));
					responseVOTemp.setStatus(status);
					responseVOTemp.setResult(null);
				}
				if (counter == 0) {
					return response(200, "SUCCESS", "", result);
				} else {
					return response(400, "ERROR", "error while linking components", result);
				}
			}
			return response(400, "ERROR", invalidCompId, result);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || processComponentPrimaryFlag()", e);
			return response(500, "ERROR", internalServerError, result);
		}
	}
	
	public Response<Object> deactivateComponent(ComponentBean component, BindingResult bindingResult,
			HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : deactivateComponent() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, component);
			}
			boolean deactivatestatus = compDAO.deactivateComponentAssociation(userName, clientId,
					component.getElementInstanceId(), "");
			if (deactivatestatus) {
				return response(200, "SUCCESS", "", deactivatestatus);
			} else {
				return response(400, "ERROR", invalidCompId, component);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || deactivateComponent()", e);
			return response(500, "ERROR", internalServerError, component);
		}
	}

	public Response<Object> getComponentBlackline(Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			String compType = reqParams.get("compType").toString();
			String sourceId = reqParams.get("sourceId").toString();
			String blckLineWith = reqParams.get("blacklineWithId").toString();
			ArrayList<String> blacklineXmlList = new ArrayList<String>();
			String result = null;
			if (CheckString.isValidString(compType) && CheckString.isNumericValue(sourceId)
					&& CheckString.isNumericValue(blckLineWith)) {
				String sourceBody = "";
				String destBody = "";
				if (compType.equalsIgnoreCase("table")) {
					sourceBody = compDAO.getTableData(sourceId, null, null).getBody();
					destBody = compDAO.getTableData(blckLineWith, null, null).getBody();
					blacklineXmlList.add(sourceBody);
					blacklineXmlList.add(destBody);
					result = CheckString.getBlackLiningXML(
							CheckString.replaceStringX(blacklineXmlList.get(0), "<table ", "<table border=\"1\" "),
							CheckString.replaceStringX(blacklineXmlList.get(1), "<table ", "<table border=\"1\" "));
					result = CheckString.xdocBookToHtml(result);
				} else {
					sourceBody = compDAO.getComponentData(sourceId).getBody().replaceAll("<para/>", "");
					destBody = compDAO.getComponentData(blckLineWith).getBody().replaceAll("<para/>", "");
					blacklineXmlList.add(sourceBody);
					blacklineXmlList.add(destBody);
					result = CheckString.getBlackLiningXML(blacklineXmlList.get(0), blacklineXmlList.get(1));
				}
				result = convertBlacklineContent(result, compType);
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode objectNode = mapper.createObjectNode();
				objectNode.put("blackline_text", result);
				objectNode.put("source_body", getConvertedBody(sourceBody, compType));
				return response(200, "SUCCESS", "", objectNode);
			}
			return response(400, "ERROR", invalidCompId, reqParams);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentBlackline()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}
	public Response<Object> getCreateComponentBlackline(Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			String searchBody = reqParams.get("searchBody");
			String detail = reqParams.get("detail");
			String compType =  reqParams.get("compType");
			Map<String,Object>	otherParams = new HashMap<>();
			ArrayList<String> blacklineXmlList = new ArrayList<String>();
			String result = null;
			String sourceBody = "";
			String destBody = "";
			ComponentBean	componentObj = new ComponentBean();
			componentObj.setBody(searchBody);
			componentObj.setType(compType);
			destBody = detail.replaceAll("<para/>", "");
			sourceBody = processComponentBody(componentObj, otherParams, request, false);
			blacklineXmlList.add(sourceBody);
			blacklineXmlList.add(destBody);
			result = CheckString.getBlackLiningXML(blacklineXmlList.get(0), blacklineXmlList.get(1));
			result = convertBlacklineContent(result, compType);
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode objectNode = mapper.createObjectNode();
			objectNode.put("blackline_text", result);
			objectNode.put("source_body", searchBody);
			return response(200, "SUCCESS", "", objectNode);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getCreateComponentBlackline()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}
	public Response<Object> getVersionBlackline(Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			String compType = reqParams.get("compType").toString();
			String shadowId = reqParams.get("shadowId").toString();
			String blacklineXml = "";
			if (CheckString.isValidString(compType) && CheckString.isNumericValue(shadowId)) {
				List<String> versionBlackline = compDAO.getVersionBlkLining(user, shadowId);
				if (versionBlackline.size() == 1) {
					blacklineXml = (String) versionBlackline.get(0);
				} else if (versionBlackline.size() == 2) {
					String oldXML = (String) versionBlackline.get(0);
					String newXML = (String) versionBlackline.get(1);
					if ((oldXML.contains("<para/>") && newXML.contains("<bridgehead>"))
							|| (oldXML.contains("<bridgehead>") && newXML.contains("<para/>"))) {
						blacklineXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
								+ "<span style=\"color:red;\">Blackline cannot be generated for these two versions.</span>";
					} else {
						if (oldXML.contains("<para/>")) {
							oldXML = oldXML.replaceAll("<para/>", "");
						}
						if (newXML.contains("<para/>")) {
							newXML = newXML.replaceAll("<para/>", "");
						}
						blacklineXml = CheckString.getBlackLiningXML(oldXML, newXML);
						blacklineXml = convertBlacklineContent(blacklineXml, compType);
					}
					ObjectMapper mapper = new ObjectMapper();
					ObjectNode objectNode = mapper.createObjectNode();
					objectNode.put("blackline_text", blacklineXml);
					objectNode.put("source_body", getConvertedBody(newXML, compType));
					objectNode.put("version_source_body", getConvertedBody(oldXML, compType));
					return response(200, "SUCCESS", "", objectNode);
				}
			}
			return response(400, "ERROR", "invalid request params", reqParams);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getVersionBlackline()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}

	public Response<Object> getVariables(Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String scope = reqParams.get("scope");
			if (CheckString.isValidString(user) && CheckString.isValidString(client)) {
				List<VariableBean> variables = compDAO.getVariables(user, client, null, scope);
				return response(200, "SUCCESS", "", variables);
			}
			return response(400, "ERROR", "user info required", reqParams);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getVariables()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}
	
	public Response<Object> getImages(Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			if (CheckString.isValidString(user) && CheckString.isValidString(client)) {
				List<ComponentBean> images = compDAO.getImages(user, client, "graphic", null, null, null, null, null);
				return response(200, "SUCCESS", "", images);
			}
			return response(400, "ERROR", "user info required", reqParams);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getImages()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}

	public Response<Object> getInlineStyles(Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String compType = reqParams.get("compType");
			if (CheckString.isValidString(user) && CheckString.isValidString(client)) {
				List<StyleBean> styles = compDAO.getInlineStyles(user, client, compType);
				return response(200, "SUCCESS", "", styles);

			}
			return response(400, "ERROR", "user info required", reqParams);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getInlineStyles()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}

	public Response<Object> addToFavoriteLibrary(String selectedComponents, HttpServletRequest request){
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : addToFavoriteLibrary() ||");
			try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			int clientId = Integer.parseInt(request.getParameter("client").toString());
			String message = compDAO.addToFavoriteLibrary(userName, clientId, selectedComponents,Integer.parseInt(favlimit));
			if (message.equalsIgnoreCase("SUCCESS")) {
				return response(200,"SUCCESS","",selectedComponents);
			} else if (message.equalsIgnoreCase("SOME COMPONENTS ALREADY EXISTS")) {
				return response(409,"ERROR",message,selectedComponents);
			}  //if (message.indexOf("Exceeded") != -1) 
				return response(403,"ERROR",message,selectedComponents);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || addToFavoriteLibrary()", e);
			return response(500,"ERROR",internalServerError,selectedComponents);
		}
	}
	
	public Response<Object> getLibraryFavorite(HttpServletRequest request){
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getLibraryFavorite() ||");
		List searchresultlist = new ArrayList();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			searchresultlist = (ArrayList) compDAO.getFavorites(userName, clientId, 0, 50);
			return response(200,"SUCCESS","",searchresultlist);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getLibraryFavorite()", e);
			return response(500,"ERROR",internalServerError,searchresultlist);
		}
	}
	
	public Response<Object> createUserFilter(Map<String, Object> reqParams, HttpServletRequest request, Filter filter,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createUserFilter() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString().toLowerCase();
			String client = request.getParameter("client").toString();
			// String client = reqParams.get("client").toString();
			// Filter filter = (Filter) reqParams.get("filter");
			SearchCriteria criteria = filter.getCriteria();
			// BindingResult bindingResult = (BindingResult) reqParams.get("bindingResult");
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, filter);
			}
			ObjectMapper mapper = new ObjectMapper();
			String criteriaVal = mapper.writeValueAsString(criteria);
			Map<String, String> createStatus = compDAO.createUserFilter(filter.getCreator().toLowerCase(), client, filter,
					criteriaVal, searchType);
			if (!CheckString.isValidString(createStatus.get("id"))) {
				status.setStatusCode(400);
				status.setMessage(createStatus.get("statusMessage"));
				status.setRequire(getErrorAttributeList(bindingResult));
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(filter);
			} else if (Integer.parseInt(createStatus.get("id")) == 0) {
				return response(409, "ERROR", createStatus.get("statusMessage"), filter);
			}
			return response(200, "SUCCESS", createStatus.get("statusMessage"), createStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createUserFilter()", e);
			return response(500, "ERROR", internalServerError, filter.getCriteria());
		}
	}

	public Response<Object> editUserFilter(Map<String, Object> reqParams, HttpServletRequest request, Filter filter,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editUserFilter() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString().toLowerCase();
			String client = request.getParameter("client").toString();
			reqParams.get("filterId");
			// String filterId = reqParams.get("filterId").toString();
			// Filter filter = (Filter) reqParams.get("filter");
			filter.setId(reqParams.get("filterId").toString());
			SearchCriteria criteria = filter.getCriteria();
			// BindingResult bindingResult = (BindingResult) reqParams.get("bindingResult");
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, filter);
			}
			ObjectMapper mapper = new ObjectMapper();
			String criteriaVal = mapper.writeValueAsString(criteria);
			String updateStatus = compDAO.updateFilterSearch(filter.getCreator().toLowerCase(), userName, client, filter, criteriaVal,
					searchType);
/*			if (updateStatus.equals("FILTER NOT EXIST")) {
				return response(409, "ERROR", updateStatus, filter);
			}*/
			if (updateStatus.equals("FILTER NOT EXIST")) {
				return response(400, "ERROR", updateStatus, filter);
			}
			return response(200, "SUCCESS", updateStatus, updateStatus);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editUserFilter()", e);
			return response(500, "ERROR", internalServerError, filter);
		}
	}

	public Response<Object> removeFromFavoriteLibrary(String selectedComponents, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : addToFavoriteLibrary() ||");

		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			int clientId = Integer.parseInt(request.getParameter("client").toString());
			String message = compDAO.removeFavoriteLibrary(userName, clientId, selectedComponents);

			if (message != "") {
				if (message == "error" || message.equals("error")) {
					return response(400, "ERROR", "error occured while removing components from favorites",
							selectedComponents);
				} else {
					return response(200, "SUCCESS", "Components removed from favorites successfully",
							selectedComponents);
				}
			}
			return response(500, "ERROR", internalServerError, selectedComponents);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || removeFromFavoriteLibrary()", e);
			return response(500, "ERROR", internalServerError, selectedComponents);
		}
	}

	public Response<Object> getComponentTags(Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getComponentTags() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		String componentId;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			componentId = reqParams.get("componentId");
			// Integer.parseInt(componentId);
			List<Tag> message = compDAO.getComponentTags(userName, clientId, componentId);
			status.setStatus("SUCCESS");
			status.setStatusCode(200);
			responseVOTemp.setStatus(status);
			responseVOTemp.setResult(message);
			return responseVOTemp;
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentTags()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}

	public Response<Object> createTags(Map<String, String> reqParams, ComponentBean component,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createTags() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		String componentId;
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			componentId = reqParams.get("componentId");
			List<Tag> tagAssociationList = component.getTagAssociation();
			Map<String, Object> result = new HashMap<String, Object>();
			Map<String, Object> taglist = new HashMap<String, Object>();
			StringBuffer associateTagIDs = new StringBuffer();
			for (Tag tag : tagAssociationList) {//Tag create process
				if (CheckString.isValidString(tag.getName()) && tag.getName().contains(" ")) {			
					return response(400,"ERROR","Invalid tag name",tag);
				} else{  
					  if(tag.getId()==0 && CheckString.isValidString(tag.getName())) {//if name only exist then create new tag
						  String creatStatus = compDAO.createTag(userName, clientId, tag.getName());
						  if (creatStatus.equals("0")) {							  
							  taglist.put(tag.getName(), "already exist");
						  }else {
							  taglist.put(tag.getName(), creatStatus);
						  }
						  tag.setId(CheckString.getInt(creatStatus));
					  }
					  if(tag.getId() > 0)
						  associateTagIDs.append(tag.getId()+",");
				}
			}  
			result.put("taglist", taglist);
			if (CheckString.isValidString(componentId)) {//Tag association to element part
				if(CheckString.isNumericValue(componentId)) {					
					Map<String, Object> associationStatus = compDAO.setTagAssociation(userName, CheckString.getInt(clientId), componentId,  associateTagIDs.toString().replaceAll(",$", ""));
					if (associationStatus.get("tagAssociationStatus").equals("INSERT SUCCESSFUL") || associationStatus.get("tagAssociationStatus").equals("DELETE SUCCESSFUL")) {
						result.put("tagAssociationStatus", "tag association update successfull");
					} else {
						result.put("tagAssociationStatus", associationStatus.get("tagAssociationStatus"));
					}					
				}else {
					return response(400,"ERROR","Component id must be numeric.",result);
				}				
			}
			return response(200,"SUCCESS","",result);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createTags()", e);
			return response(500,"ERROR",internalServerError,component);
		}
	}

	public Response<Object> setComponentTags(ComponentBean component, BindingResult bindingResult,
			Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : setComponentTags() ||");
		String componentId = reqParams.get("componentId");
		JSONParser parser = new JSONParser();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, component);
			}

			component.setElementInstanceId(componentId);
			return response(200, "SUCCESS", "",
					(org.json.simple.JSONObject) parser.parse(setTagAssociation(userName, clientId, component)));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || setComponentTags()", e);
			return response(500, "ERROR", internalServerError, component);
		}
	}

	public Response<Object> createLinkageComponent(ComponentBean component, BindingResult bindingResult,
			Map<String, String> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createLinkageComponent() ||");
		String componentId = reqParams.get("componentId");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, component);
			}
			if (CheckString.isNumericValue(componentId)) {
				component.setPrimaryComponentId(componentId);
				String checkComponentExist = compDAO.checkComponentName(userName, clientId, "0", component.getName());
				if (checkComponentExist != null && !checkComponentExist.trim().equalsIgnoreCase("[]")) {
					return response(409, "ERROR", duplicate, component);
				} else if (checkComponentExist != null && checkComponentExist.trim().equalsIgnoreCase("[]")) {
					String elementInstanceId = compDAO.createSecondaryComponent(userName, clientId, component);
					if (elementInstanceId == null || elementInstanceId.equalsIgnoreCase("")) {
						return response(400, "ERROR", "error occured while creating secondary component", component);
					} else {
						component.setElementInstanceId(elementInstanceId);
						return response(200, "SUCCESS", "Components created successfully", component);
					}
				}
			}
			return response(400, "ERROR", "Invalid Component ID", componentId);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createLinkageComponent()", e);
			return response(500, "ERROR", internalServerError, componentId);
		}
	}

	public Response<Object> getUserFiltesr(HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			if (CheckString.isValidString(user)) {
				List<Filter> filterlist = compDAO.getUserFilters(user, client, searchType);

				if (filterlist.size() > 0) {
					Collections.sort(filterlist, new Comparator<Filter>() {
						@Override
						public int compare(final Filter object1, final Filter object2) {
							return object1.getName().compareTo(object2.getName());
						}
					});
				}
				return response(200, "SUCCESS", "", filterlist);
			}
			return response(400, "ERROR", "creator name required", user);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentBDO || getUserFiltesr()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}

	public Response<Object> getTableTypes(Map<String, String> pathParam, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (!CheckString.isValidString(userName) || !CheckString.isNumericValue(clientId)) {
				status.setStatus("ERROR");
				status.setStatusCode(400);
				status.setMessage("required fields in path param");
				status.setRequire(new StringBuffer("user,client"));
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(null);
				return responseVOTemp;
			}
			List<TableType> tableTypes = compDAO.getTableTypes(userName, clientId, pathParam.get("doctype"));
			/*
			 * Map<String, List<TableType>> doctypeTableTypes = new HashMap<String,
			 * List<TableType>>(); for(TableType tableType : tableTypes) {
			 * if(doctypeTableTypes.containsKey(tableType.getDocType())) {
			 * doctypeTableTypes.get(tableType.getDocType()).add(tableType); }else {
			 * List<TableType> tempTType = new ArrayList<TableType>();
			 * tempTType.add(tableType); doctypeTableTypes.put(tableType.getDocType(),
			 * tempTType); } }
			 */
			return response(200, "SUCCESS", "", tableTypes);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getTableTypes()", e);
			return response(500, "ERROR", internalServerError, pathParam);
		}
	}

	public Response<Object> getElementStylesList(HttpServletRequest request, String type) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (!CheckString.isValidString(userName) || !CheckString.isNumericValue(clientId)) {
				status.setStatus("ERROR");
				status.setStatusCode(400);
				status.setMessage("required fields in path param");
				status.setRequire(new StringBuffer("user,client"));
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(null);
				return responseVOTemp;
			}
			List<SelectBean> elementStyleList = compDAO.getElementStylesList(userName, clientId, "TABLE", "ROW", type,
					null, null);
			return response(200, "SUCCESS", null, elementStyleList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getElementStylesList()", e);
			return response(500, "ERROR", internalServerError, type);
		}
	}

	public Response<Object> getComponentTypes(HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		List<ComponentPermissionBean> compTypeList = new ArrayList<ComponentPermissionBean>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			if (!CheckString.isValidString(userName) || !CheckString.isNumericValue(clientId)) {
				status.setStatus("ERROR");
				status.setStatusCode(400);
				status.setMessage("required fields in path param");
				status.setRequire(new StringBuffer("user,client"));
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(null);
				return responseVOTemp;
			}
			Map<String, ComponentPermissionBean> compPermission = userPrevilege.getComponentPrivilege(userName,
					clientId);
			for (String key : compPermission.keySet()) {
				if (key.equalsIgnoreCase("text")) {
					compPermission.get(key).setInternalName(text);
					compTypeList.add(compPermission.get(key));
				} else if (key.equalsIgnoreCase("subhead")) {
					compPermission.get(key).setInternalName(subhead);
					compTypeList.add(compPermission.get(key));
				} else if (key.equalsIgnoreCase("note")) {
					compPermission.get(key).setInternalName(key);
					compTypeList.add(compPermission.get(key));
				} else if (key.equalsIgnoreCase("footnote")) {
					compPermission.get(key).setInternalName(key);
					compTypeList.add(compPermission.get(key));
				} else if (key.equalsIgnoreCase("variable")) {
					compPermission.get(key).setInternalName(variable);
					compTypeList.add(compPermission.get(key));
				} else if (key.equalsIgnoreCase("image")) {
					compPermission.get(key).setInternalName(image);
					compTypeList.add(compPermission.get(key));
				} else if (key.equalsIgnoreCase("table")) {
					compPermission.get(key).setInternalName("table");
					compTypeList.add(compPermission.get(key));
				}
				continue;
			}
			if (compTypeList.size() > 0) {
				Collections.sort(compTypeList, new Comparator<ComponentPermissionBean>() {
					@Override
					public int compare(final ComponentPermissionBean object1, final ComponentPermissionBean object2) {
						return object1.getType().compareTo(object2.getType());
					}
				});
			}
			return response(200, "SUCCESS", "", compTypeList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentTypes()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	public ComponentBean checkComponentType(ComponentBean component) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		ComponentBean compDataFromDB =null;
		try {
			compDataFromDB =  compDAO.getComponentData(component.getElementInstanceId());
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentBDO || checkComponentType()", e);
		}
		return compDataFromDB;
	}

	public Response<Object> updateSecondaryCompStatus(String componentId, ComponentBean component,
			HttpServletRequest request, BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog
				.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : updateSecondaryCompStatus() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			// String clientId = reqParams.get("client").toString();
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, component);
			}
			return response(200, "SUCCESS", "",
					compDAO.setComponentStatus(userName, componentId, component.getStatusId(), "Y", "Comment"));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || updateSecondaryCompStatus()", e);
			return response(500, "ERROR", internalServerError, component);
		}
	}

	public Response<Object> createContext(ContextBean contextBean, HttpServletRequest request,BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createContext() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();

			// String clientId = reqParams.get("client").toString();
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, contextBean);
			}
			ArrayList<String> Status = compDAO.createEditContext(user, client, contextBean.getName(), null);
			return response(200, "SUCCESS", "", Status);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createContext()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}

	public Response<Object> editContext(String contextId, ContextBean contextBean, BindingResult bindingResult,
			Map<String, Object> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editContext() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();

			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, contextBean);
			}
			ArrayList<String> Status = compDAO.createEditContext(user, client, contextBean.getName(), contextId);
			return response(200, "SUCCESS", "", Status);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editContext()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	
	public Response<Object> getFootnoteList(Map<String, Object> reqParams, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getFootnoteList() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();

			String componentId = reqParams.get("componentId") != null ? (String) reqParams.get("componentId") : null;
			List<ComponentBean> footnoteList = compDAO.getGlobalElementQualListContext(user, client, "footnote", null,
					null, null, null, componentId);

			ArrayList<FootNoteAttributes> footnoteAttributes = (ArrayList<FootNoteAttributes>) compDAO
					.getFootnoteDetails(user, client, null);
			List<Order> orderlist = new ArrayList();
			ArrayList order_attr = new ArrayList();

			List<Placement> placementlist = new ArrayList();
			ArrayList placement_attr = new ArrayList();

			List<Style> stylelist = new ArrayList();
			ArrayList style_attr = new ArrayList();

			for (int footnoteAttributesIndex = 0; footnoteAttributesIndex < footnoteAttributes
					.size(); footnoteAttributesIndex++) {
				FootNoteAttributes f_att = footnoteAttributes.get(footnoteAttributesIndex);

				orderlist = f_att.getOrder();
				// order_attr = f_att.getOrder_attr();

				placementlist = f_att.getPlacement();
				// placement_attr = f_att.getPlacement_attr();

				stylelist = f_att.getStyle();
				// style_attr = f_att.getStyle_attr();
			}
			for (int idx = 0; idx < footnoteList.size(); idx++) {
				footnoteList.get(idx).setOrderList(orderlist);
				footnoteList.get(idx).setPlacementList(placementlist);
				footnoteList.get(idx).setStyleList(stylelist);
			}
			return response(200, "SUCCESS", "", footnoteList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getFootnoteList()", e);
			return response(500, "ERROR", internalServerError, reqParams);
		}
	}

	public Response<Object> fundListonAssetClass(Map<String, Object> reqParams, ComponentBean componentBean,
			HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : fundListonAssetClass() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();

			List<Fund> fundList = compDAO.getFundListOnAssetClass(user, client, componentBean.getAssetClass());
			return response(200, "SUCCESS", null, fundList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || fundListonAssetClass()", e);
			return response(500, "ERROR", internalServerError, componentBean);
		}
	}

	public Response<Object> getComponentStatusList(Map<String, String> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getComponentStatusList() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String statusId = pathVariablesMap.get("statusId");
			// String schemaId = pathVariablesMap.get("schemaId").toString();
			List<ComponentStatus> compStatusList = compDAO.getComponentStatusList(user, client, statusId, null);
			return response(200, "SUCCESS", null, compStatusList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentStatusList()", e);
			return response(500, "ERROR", internalServerError, pathVariablesMap);
		}
	}

	public Response<Object> getAuthUsers(HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getAuthUsers() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			List<UserBean> userList = compDAO.getAuthUsers(user, client);
			return response(200, "SUCCESS", "", userList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getAuthUsers()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}

	public Response<Object> bulkdataUpdate(HttpServletRequest request, ComponentBean component) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : bulkdataUpdate() ||");
		ComponentBean componentBulkObj = null;
		List<ComponentBean> componentBeanList = new ArrayList<ComponentBean>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();

			String elementChildId[] = component.getElementInstanceId().split(",");
			// String statusIds[] = component.getStatusId().split(",");
			// String contexts[] = component.getContext().split(",");

			if (elementChildId.length > 0) { 
				for (int i = 0; i < elementChildId.length; i++) {
					componentBulkObj = new ComponentBean();
					componentBulkObj.setElementInstanceId(elementChildId[i]);
					componentBulkObj.setFcomp_assignee(component.getFcomp_assignee());
					componentBulkObj.setStatusId(component.getStatusId());
					if (CheckString.isValidString(component.getWorkFlowStatusId())
							&& !CheckString.isValidString(component.getFcomp_assignee())) {
						WorkFLStatus workFLStatus = compDAO.getStatusDefaultAssignee(user, client,
								component.getWorkFlowStatusId(), schema);
						if (CheckString.isValidString(workFLStatus.getAssignee())) {
							componentBulkObj.setFcomp_assignee(workFLStatus.getAssignee());
						}
					} else {
						componentBulkObj.setFcomp_assignee(component.getFcomp_assignee());
					}
					componentBulkObj.setWorkFlowStatusId(component.getWorkFlowStatusId());
					componentBulkObj.setContext(component.getContext());
					componentBulkObj.setEffectiveDate(component.getEffectiveDate());
					componentBulkObj.setExpirationDate(component.getExpirationDate());
					componentBulkObj.setLocale(component.getLocale());
					componentBulkObj.setParentContextId(component.getParentContextId());
					componentBeanList.add(componentBulkObj);
				}
			}
			Map<String, String> editCompStatusMap = compDAO.bulkdataUpdate(user, client, componentBeanList);
			return response(200, "SUCCESS", "", "Success");
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || bulkdataUpdate()", e);
			return response(500, "ERROR", internalServerError, component);
		}
	}

	public Response<Object> getAffectedComponent(String fimportstatusid, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getAffectedComponent() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			List<ComponentBean> affectedComponents = compDAO.getAffectedComponent(user, client, fimportstatusid);

			return response(200, "SUCCESS", "", affectedComponents);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getAffectedComponent()", e);
			return response(500, "ERROR", internalServerError, fimportstatusid);
		}
	}

	public Response<Object> revertToVersion(ComponentBean component, HttpServletRequest request,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : revertToVersion() ||");
		Map<String, Object> updateProcessStatus = new HashMap<String, Object>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, component);
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();

			ComponentBean componentBean = compDAO.getComponentData(component.getElementInstanceId());
			component.setType(componentBean.getType());
			component.setBody(compDAO.getQualDataByShadowId(user, component.getShadowId()));
			component.setOldFootnoteIds(getHiddenValues(component.getBody(), "footnote"));
			component.setFootnoteIds(getHiddenValues(component.getBody(), "footnote"));

			component.setRecursiveIds(getHiddenValues(component.getBody(), "embedded"));
			component.setOldRecursiveIds(getHiddenValues(component.getBody(), "embedded"));

			component.setEntityIds(getHiddenValues(component.getBody(), "userVariable"));
			component.setOldEntityIds(getHiddenValues(component.getBody(), "userVariable"));

			component.setSystemEntityIds(getHiddenValues(component.getBody(), "systemVariable"));
			component.setOldSystemEntityIds(getHiddenValues(component.getBody(), "systemVariable"));

			// component.setBody(getConvertedBody(component.getBody(),
			// component.getType()));
			developerLog.debug("Reveted body:" + component.getBody());
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("componentObj", component);
			updateProcessStatus.put("updateStatus", compDAO.editComponentField(user, client, paramMap));
			return response(200, "SUCCESS", "", updateProcessStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || revertToVersion()", e);
			return response(500, "ERROR", internalServerError, component);
		}
	}
	
	public Response<Object> getContentCatByGroup(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getContentCatWithPermission() ||");
		List<ContextBean> contextBeansList = new ArrayList<ContextBean>();
		List<ComponentStatus> subcategoryList = null;
		List<ContextBean> finalContextBeansList = new ArrayList<ContextBean>();
		ContextBean contextBeanObj;
		ComponentStatus componentStatus;
		int i=0;
		Map<Integer, String> categoryMap = new LinkedHashMap<Integer, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String groupId = pathVariablesMap.get("groupId");
			contextBeansList = compDAO.getContentCatByGroup(user, client, groupId);
				for (ContextBean contextBean : contextBeansList) {
					categoryMap.put(contextBean.getId(), "");
				}
				
			for (Map.Entry<Integer, String> entry : categoryMap.entrySet()) {
				contextBeanObj = new ContextBean();
				subcategoryList = new ArrayList<ComponentStatus>();
				for (ContextBean contextBeanTemp : contextBeansList) {
					if (entry.getKey().equals(contextBeanTemp.getId())) {
						    componentStatus = new ComponentStatus();
							contextBeanObj.setId(entry.getKey());
							contextBeanObj.setOrder(contextBeanTemp.getOrder());
							contextBeanObj.setName(contextBeanTemp.getName());
						if (CheckString.isValidString(contextBeanTemp.getGroupIdAssociation())) {
							if (contextBeanTemp.getGroupIdAssociation().equals("Y")) {
								componentStatus.setPermission(true);
								contextBeanObj.setPermission(true);
							}
						}
							componentStatus.setName(contextBeanTemp.getSname());
							componentStatus.setOrder(contextBeanTemp.getsOrder());
							componentStatus.setsId(contextBeanTemp.getsId());

							if (!componentStatus.getsId().equals("0")) {
								subcategoryList.add(componentStatus);	
							}
					}
					if (subcategoryList.size() != 0) {
						contextBeanObj.setSubCatogory(subcategoryList);
					}
				}
				if (contextBeanObj.getId() != 0) {
					finalContextBeansList.add(contextBeanObj);
				}
			}
				return response(200, "SUCCESS", "", finalContextBeansList);
			
		
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getContentCategory()", e);
			return response(500, "ERROR", internalServerError, pathVariablesMap);
		}
	}
	public Response<Object> getContentCategory(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getContentCategory() ||");
		List<ContextBean> contextBeansList = new ArrayList<ContextBean>();
		List<ComponentStatus> subcategoryList = null;
		List<ContextBean> finalContextBeansList = new ArrayList<ContextBean>();
		ContextBean contextBeanObj;
		ContextSchema schema;
		ComponentStatus componentStatus;
		int i=0;
		Map<Integer, String> categoryMap = new HashMap<Integer, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String categoryId = pathVariablesMap.get("categoryId");
				//schema = new ContextSchema();
				contextBeansList = compDAO.getContentCategory(user, client, categoryId);
				for (ContextBean contextBean : contextBeansList) {
					categoryMap.put(contextBean.getId(), "");
				}
				for (Map.Entry<Integer, String> entry : categoryMap.entrySet()) {
					contextBeanObj = new ContextBean();
					subcategoryList = new ArrayList<ComponentStatus>();
					for (ContextBean contextBeanTemp : contextBeansList) {
						if (entry.getKey().equals(contextBeanTemp.getId())) {
							componentStatus = new ComponentStatus();
							contextBeanObj.setId(entry.getKey());
							contextBeanObj.setOrder(contextBeanTemp.getOrder());
							contextBeanObj.setName(contextBeanTemp.getName());
							componentStatus.setName(contextBeanTemp.getSname());
							componentStatus.setOrder(contextBeanTemp.getsOrder());
							componentStatus.setsId(contextBeanTemp.getsId());
							if (componentStatus.getsId().equals("0")) {
								// subcategoryList.add(componentStatus);
							} else {
								subcategoryList.add(componentStatus);
							}
						}
						if (subcategoryList.size() != 0) {
							contextBeanObj.setSubCatogory(subcategoryList);
						}
					}
					finalContextBeansList.add(contextBeanObj);
				}
			//	schema.setId("1");schema.setName("Default");schema.setContext(finalContextBeansList);
				return response(200, "SUCCESS", "", finalContextBeansList);
			
		
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getContentCategory()", e);
			return response(500, "ERROR", internalServerError, pathVariablesMap);
		}
	}

	public Response<Object> availableToPrimary(Map<String, String> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : availableToPrimary() ||");
		List<ComponentStatus> subcategoryList = null;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String categoryId = pathVariablesMap.get("categoryId");
			subcategoryList = compDAO.availableToPrimary(user, client,categoryId);
			return response(200, "SUCCESS", "", subcategoryList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || availableToPrimary()", e);
			return response(500, "ERROR", internalServerError, pathVariablesMap);
		}
	}
	public Response<Object> createPrimarySecondaryCatogory(Map<String, String> pathVariablesMap,ContextBean contextBean, HttpServletRequest request,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createPrimarySecondaryCatogory() ||");
		Map<String,String> result = new HashMap<String,String>();
		List<String> subCatIds = new ArrayList<String>();
		boolean flag = false;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String categoryId = pathVariablesMap.get("categoryId");
			if(!CheckString.isValidString(categoryId)){
				result = compDAO.createPrimaryCatogory(user, client, null, contextBean);
				if(result.get("createStatus").equals("CONTEXT ALREADY EXIST")) {
					return response(409, "ERROR", "Context category already Exist", result);
				}else {
					 String catId = result.get("id");
					 String order = result.get("order");
					 contextBean.getSchemaId();
					 
				}
				return response(200, "SUCCESS", "", result);
			}
			//subCategory
			if(CheckString.isValidString(categoryId)){
				List<ComponentStatus> subCategoryList = contextBean.getSubCatogory();
				for (ComponentStatus componentStatus : subCategoryList) {
					result = compDAO.createSecondaryCatogory(user, client, categoryId, componentStatus);
					if(result.get("createStatus").equals("MAPPING ALREADY EXIST")) {
						flag = true;
					}
					subCatIds.add(result.get("id"));
				}
				if(flag) {
					return response(409, "ERROR", "Some Categories already exist", subCatIds);
				}
				return response(200, "SUCCESS", "", subCatIds);
			}
			return response(500, "ERROR", internalServerError, contextBean);
			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createPrimarySecondaryCatogory()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	public Response<Object> removeCategory(ContextBean contextBean, HttpServletRequest request,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : removeCategory() ||");
		String removeCatStatus = null;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
						
			if(CheckString.isValidString(contextBean.getsId())) {
				String contentIds[] = contextBean.getsId().split(",");
				for (int i = 0; i < contentIds.length; i++) {
					removeCatStatus = compDAO.removeCategory(user, client, contextBean.getIds(),contentIds[i]);
				}
				return response(200, "SUCCESS", "Delete Succesfull", contextBean);
			}else {
				String contentIds[] = contextBean.getIds().split(",");
				for (int i = 0; i < contentIds.length; i++) {
				removeCatStatus = compDAO.removeCategory(user, client,contentIds[i],null);
				}
				return response(200, "SUCCESS", "Delete Succesfull", contextBean);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || removeCategory()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	public Response<Object> addPermissionToContentCategory(ContextBean contextBean, HttpServletRequest request,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : addPermissionToContentCategory() ||");
		Map<String, String> updateProcessStatus = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			//get permission for the user
			//check permission for the user
			//if yes edit
			//else return error
		    updateProcessStatus = compDAO.addPermissionToContentCategory(user, client,contextBean);
			return response(200, "SUCCESS", "", "success");
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || addPermissionToContentCategory()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	public Response<Object> authEditSubContentCatogory(ContextBean contextBean, HttpServletRequest request,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : authEditSubContentCatogory() ||");
		Map<String, String> updateProcessStatus = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			//get permission for the user
			//check permission for the user
			//if yes edit
		    //else return error
		    //updateProcessStatus = compDAO.editAuthContentCategory(user, client,contextBean.getId(), contextBean);
			return response(200, "SUCCESS", "", updateProcessStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || authEditSubContentCatogory()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	public Response<Object> editContentCategory(ContextBean contextBean, HttpServletRequest request,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editContentCategory() ||");
		Map<String, String> updateProcessStatus = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			//String categoryId = pathVariablesMap.get("categoryId");
			updateProcessStatus = compDAO.editContentCategory(user, client,contextBean.getId(), contextBean);
			if(updateProcessStatus.get("updateStatus").trim().equals("ELEMENT DESCRIPTON ALREADY EXISTED")) {
				return response(409, "ERROR", "Category already Exist", updateProcessStatus);
			}
			return response(200, "SUCCESS", "", updateProcessStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editContentCategory()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	public Response<Object> editSubContentCategory(Map<String, String> pathVariablesMap, ContextBean contextBean,
			HttpServletRequest request, BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editSubContentCategory() ||");
		Map<String, String> updateProcessStatus = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String categoryId = pathVariablesMap.get("subCategoryId");
			updateProcessStatus = compDAO.editSubContentCategory(user, client, categoryId, contextBean);
			if(updateProcessStatus.get("updateStatus").trim().equals("ELEMENT DESCRIPTON ALREADY EXISTED")) {
				return response(409, "ERROR", "Category already Exist", updateProcessStatus);
			}
			return response(200, "SUCCESS", "", updateProcessStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editSubContentCategory()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	public Response<Object> orderContentCatogory(ContextBean contextBean,HttpServletRequest request, BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editSubContentCategory() ||");
		Map<String, String> updateProcessStatus = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			//for (ContextBean contextBean2 : contextBean) {
				updateProcessStatus = compDAO.orderContentCatogory(user, client,contextBean);
			//}
			return response(200, "SUCCESS", "", updateProcessStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editSubContentCategory()", e);
			return response(500, "ERROR", internalServerError, contextBean);
		}
	}
	
	
	
	public Response<Object> createCategorySchema(ContextSchema contextSchema, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createCategorySchema() ||");
		Map<String, String> creatStatus = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			creatStatus = compDAO.createCategorySchema(user, client,contextSchema);
			return response(200, "SUCCESS", "", creatStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createCategorySchema()", e);
			return response(500, "ERROR", internalServerError, contextSchema);
		}	
	}
	public Response<Object> getShareClassByFund(Map<String, String> pathVariablesMap, 
			HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getShareClassByFund() ||");
		List<ShareClass> shareClassList = new ArrayList<ShareClass>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String fundId = pathVariablesMap.get("fundId");
			shareClassList = compDAO.getShareClassByFund(user, client,fundId);
			return response(200, "SUCCESS", "", shareClassList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getShareClassByFund()", e);
			return response(500, "ERROR", internalServerError ,"Error while getting funds shareClasses");
		}
	}
	
	public Response<Object> resolveComponentVariables(Map<String, String> pathVariablesMap, ResloveVarBean resloveVarBean,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : resolveComponentVariables() ||");
		Map<String, String> resolveStatus = new HashMap<String, String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String componentId = pathVariablesMap.get("componentId");
			resolveStatus = compDAO.resolveComponentVariables(user, client,resloveVarBean,componentId);
			return response(200, "SUCCESS", "", resolveStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || resolveComponentVariables()", e);
			return response(500, "ERROR", internalServerError ,"Error while resolving ");
		}
	}
	public Response<Object> previewVariableValue(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : previewVariableValue() ||");
		List<ResloveVarBean> variableList = new ArrayList<ResloveVarBean>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String user = request.getParameter("user").toString();
			String client = request.getParameter("client").toString();
			String componentId = pathVariablesMap.get("componentId");
			variableList = compDAO.previewVariableValue(user, client,componentId);
			return response(200, "SUCCESS", "", variableList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || previewVariableValue()", e);
			return response(500, "ERROR", internalServerError ,"Error while resolving ");
		}
	}
	
	/*	/----------------------------------------------------TABLE --------------------------------------------/*/

	public Response<Object> createTableComponent(TableComponent table, BindingResult bindingResult,
			HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createTableComponent() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult, table);
			}
			table.setOperation("create");
			/*
			 * if(! userPrevilege.checkComponentPerrmission(userName, clientId,table)) {
			 * status.setStatusCode(403); status.setStatus("ERROR");
			 * status.setMessage("Insufficient Previlege");
			 * responseVOTemp.setStatus(status); responseVOTemp.setResult(table); return
			 * responseVOTemp; }
			 */
			String existingComponentResult = compDAO.checkComponentName(userName, clientId, null, table.getName());
			if (existingComponentResult != null && !existingComponentResult.trim().equalsIgnoreCase("[]")) {
				return response(409, "ERROR", duplicate, table);
			}
			/*
			 * table.setBody(CheckString.convertXHtmlToDocBook(table.getBody())); body =
			 * CheckString.replaceStringX(body, "<table>","<table rows=\"" + rows +
			 * "\" cols=\""+ columns+ "\" render=\"0\" datatype=\"" + table.getDataType()+
			 * "\" tabletype=\"" + table.getTableType()+ "\" primary=\"" +
			 * tableBean.getPrimary()+ "\" style=\"" + tableBean.getStyle() + "\">"); if
			 * (CheckString.convertXHtmlToDocBook(body).length() >= 11500) {
			 * table.setQuantData(body); table.setBody(body.substring(0,
			 * body.indexOf("<tbody>"))+
			 * "<tbody><row><entry></entry></row></tbody></table>"); }else {
			 * table.setBody(body); }
			 */

			if (CheckString.isValidString(table.getTableBody())) {
				// table.setBody(CheckString.convertXHtmlToDocBook(table.getBody()));
				table.setBody(doTableConversionOperations(table));
			}

			table.setGlobal("1");
			table.setQualDataCon("default");
			table.setFootnoteIds(getHiddenValues(table.getBody(), "footnote"));
			table.setRecursiveIds(getHiddenValues(table.getBody(), "embedded"));
			table.setEntityIds(getHiddenValues(table.getBody(), "userVariable"));
			table.setSystemEntityIds(getHiddenValues(table.getBody(), "systemVariable"));
			Map<String, String> objectNode = compDAO.createTableComponent(userName, clientId, table);
			compDAO.setComponentAssignee(userName, clientId, objectNode.get("elementId"), table);
			setComponentAssociation(userName, clientId, table);
			if (table.getTagAssociation() != null && table.getTagAssociation().size() > 0) {
				Map<String, String> tempParam = new HashMap<String, String>();
				tempParam.put("user", userName);
				tempParam.put("client", clientId);
				tempParam.put("componentId", table.getElementInstanceId());
				createTags(tempParam, table, request);
			}
			if (objectNode.get("message").toString().equals("success")) {
				table.setElementInstanceId(objectNode.get("elementId"));
				//update component Kafka Service
//        		elasticServerKafkaDAO.kafkaProducerNotifyComponentUpdation(userName, clientId, table.getElementInstanceId());
				return response(200, "SUCCESS", "", table);
			} else if (objectNode.get("message").toString().equals("texttoolong")) {
				return response(413, "ERROR", textTooLong, table);
			} else if (objectNode.get("message").toString().equals("footnoteid already exist,error")) {
				return response(409, "ERROR", duplicateFtId, table);
			} else {
				return response(500, "ERROR", internalServerError, table);
			}

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createTableComponent()", e);
			return response(500, "ERROR", internalServerError, table);
		}
	}
	
	public Response<Object> editTableComponent(String idToEdit, ComponentBean componentObj,Map<String,Object> reqParams,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.TableBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editTableComponent() ||");

		Map<String, Object> updateProcessStatus = new HashMap<String, Object>();
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			componentObj.setOperation("edit");
/*			if(CheckString.isValidString(componentObj.getType()) && ! userPrevilege.checkComponentPerrmission(userName, clientId,componentObj )) {
				status.setStatusCode(403);
				status.setStatus("ERROR");
				status.setMessage("Insufficient Previlege");
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(componentObj);	
				return responseVOTemp;
			}*/
			componentObj.setElementInstanceId(idToEdit);
			ComponentBean compDataFromDB =  compDAO.getComponentData(componentObj.getElementInstanceId());
			if(!CheckString.isValidString(componentObj.getType())) {
				componentObj.setType(compDataFromDB.getType());
			}
			//check if name is already exist
			if(CheckString.isValidString(componentObj.getName()) && CheckString.isValidString(componentObj.getElementInstanceId())) {
				String existingComponentResult = compDAO.checkComponentName(userName, clientId, componentObj.getElementInstanceId(), componentObj.getName());
				if(existingComponentResult != null && !existingComponentResult.trim().equalsIgnoreCase("[]")){
					status.setStatusCode(409);
					status.setStatus("ERROR");
					status.setMessage("Duplicate Component");
					responseVOTemp.setStatus(status); 
					responseVOTemp.setResult(componentObj);	
		        	return responseVOTemp; 
				}				
	        }
			if(CheckString.isNumericValue(componentObj.getElementInstanceId())){
				boolean isFieldUpdate = false;//this flag can be used for future reference where the legacy code has written to update the component inside the if block dao call
	    		if(CheckString.isValidString(componentObj.getName()) || CheckString.isValidString(componentObj.getBody()) || CheckString.isValidString(componentObj.getBody())|| CheckString.isValidString(componentObj.getLocale())|| CheckString.isValidString(componentObj.getContext())|| CheckString.isValidString(componentObj.getEffectiveDate())|| CheckString.isValidString(componentObj.getExpirationDate()) || CheckString.isValidString(componentObj.getTableType())|| CheckString.isValidString(componentObj.getFcomp_assignee())|| CheckString.isValidString(componentObj.getWorkFlowStatusId())|| CheckString.isValidString(componentObj.getOwner())) {
	    			isFieldUpdate = true;
    			}
	    		if(componentObj.getTableBody() != null && !componentObj.getTableBody().trim().equals("")) {
	    			isFieldUpdate = true;
	    		}
	    		if(isFieldUpdate) {	
					//ComponentBean compDataFromDB =  compDAO.getComponentData(componentObj.getElementInstanceId());
					componentObj.setQualData_Id(compDataFromDB.getQualData_Id());
					componentObj.setQualDataId(compDataFromDB.getQualDataId());
					componentObj.setLastUpdatedTime(compDataFromDB.getLastUpdatedTime());
					
					String contentChangedFlag = "false";				
					//convert component type  process
					if((CheckString.isValidString(componentObj.getConvertTo()) && !componentObj.getConvertTo().equalsIgnoreCase("-1")) && (componentObj.getType().equalsIgnoreCase("para")||componentObj.getType().equalsIgnoreCase("bridgehead"))){
						compDAO.convertComponent(userName, clientId, componentObj.getElementInstanceId(), compDataFromDB.getType(), componentObj.getConvertTo());
					}
					
					if(CheckString.isValidString(componentObj.getTableBody())) {
						componentObj.setBody(doTableConversionOperations(componentObj));//added to take the data in the old library way do the conversions accordingly
						updateProcessStatus.put("body", getConvertedBody(componentObj.getBody(),componentObj.getType()));
						//componentObj.setBody(CheckString.convertXHtmlToDocBook(componentObj.getBody()));
						componentObj.setFootnoteIds(getHiddenValues(componentObj.getBody(), "footnote"));
						componentObj.setRecursiveIds(getHiddenValues(componentObj.getBody(), "embedded"));
						componentObj.setEntityIds(getHiddenValues(componentObj.getBody(), "userVariable"));
						componentObj.setSystemEntityIds(getHiddenValues(componentObj.getBody(), "systemVariable"));
					}		
					componentObj.setGlobal("1");
					componentObj.setQualDataCon("default");	
					if(CheckString.isValidString(componentObj.getWorkFlowStatusId())) {
						WorkFLStatus workFLStatus = compDAO.getStatusDefaultAssignee(userName,clientId,componentObj.getWorkFlowStatusId(),schema);
						if(CheckString.isValidString(workFLStatus.getAssignee()) && !CheckString.isValidString(componentObj.getFcomp_assignee())) {
							componentObj.setFcomp_assignee(workFLStatus.getAssignee());
						}						
	    		     } 
		        	Map<String, Object> paramMap = new HashMap<String, Object>();
		        	paramMap.put("componentObj",componentObj);
		    		paramMap.put("overwriteStatus",(CheckString.isValidString(componentObj.getOverrideStatus())&&componentObj.getOverrideStatus().equalsIgnoreCase("y")) ? "Y" : "N");
		    		paramMap.put("qualDataCond",(CheckString.isValidString(componentObj.getQualDataCon())) ? componentObj.getQualDataCon() : "");
		    		paramMap.put("qualDataTableType",(CheckString.isValidString(componentObj.getQualDataTableType())) ? componentObj.getQualDataTableType() : "");
		    		if(CheckString.isValidString(componentObj.getBody()) && !compDataFromDB.getBody().equals(componentObj.getBody())){
		    			contentChangedFlag = "true";
		    		}
		    		paramMap.put("contentchanged",contentChangedFlag);
		        	
		    		//update component data    		    			
	    			updateProcessStatus.put("updateStatus", compDAO.editComponentField(userName, clientId, paramMap));
	    			
	    		}else {  			
	    			//updateProcessStatus.put("updateStatus", compDAO.editComponentData(userName, clientId, paramMap));for legacy code update	
	    			//set Association process
		    		if(CheckString.isValidString(componentObj.getDocumentAssociation()) || CheckString.isValidString(componentObj.getDocumentTypeAssociation()) || CheckString.isValidString(componentObj.getFundsAssociation()) || CheckString.isValidString(componentObj.getAssetClassAssociation())) {
		    			updateProcessStatus.put("associationStatus",setComponentAssociation(userName, clientId, componentObj));
		    		}	    		
		        	//set tag association
		    		if(CheckString.isValidString(componentObj.getTags())){
		    			updateProcessStatus.put("tagStatus",setTagAssociation(userName, clientId, componentObj));
		    		}
		    		//update footnote mapping
		    		if(componentObj.getType().equalsIgnoreCase("footnote")&& !componentObj.getFootnoteMapping().equals(componentObj.getPreviousFootnoteMapping())){
		    			updateProcessStatus.put("footnoteMappingStatus",processFootnoteMapping(userName, clientId, componentObj));
		    		}
	    		}			
	    		
	    		//update component Kafka Service
//        		elasticServerKafkaDAO.kafkaProducerNotifyComponentUpdation(userName, clientId, idToEdit);
	    		status.setStatusCode(200);
	    		status.setStatus("SUCCESS");
	    		responseVOTemp.setStatus(status);
	    		responseVOTemp.setResult(updateProcessStatus);	
	    		
			}else {
				status.setStatusCode(400);
				status.setStatus("ERROR");
				status.setMessage("Invalid Component ID");
				responseVOTemp.setStatus(status);
				responseVOTemp.setResult(idToEdit);	
			}
		}catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editTableComponent()", e);
			return response(500,"ERROR",internalServerError,componentObj);
		}		
        return responseVOTemp;
	}


	private String doTableConversionOperations(ComponentBean tableObj) throws Exception {
		String mergedCells = tableObj.getMergeCellInfo();
		String tableBodyString = tableObj.getTableBody();
		int rows = CheckString.getInt(tableObj.getRows());
		int cols = CheckString.getInt(tableObj.getColumns());
		String rowStyles = tableObj.getRowStyles() != null ? tableObj.getRowStyles()  : "";
		String prevRenderType = "0";//update later
		String tabletype = tableObj.getTableType();
		String tablename = tableObj.getName();
		String datatype = tableObj.getDataTypeInd();//update later
		checkTableForZeroRowZeroColumn(rows, cols, tablename, tableBodyString);
		TableDetails tb = new TableDetails(rows, cols);
		tb.setTotalcols(cols);
		tb.setTotalrows(rows);
		tb.setTableValue(rows, cols,tableBodyString);
		tb.setCellValues(tb, tableBodyString, mergedCells);
		tb.registerStyleAttributes(rowStyles.split("\\$8910"));
		tb.setGraphic(null);
/*		tb.setNewAttribute(getNewAttributeList());
		tb.setNewAddedColAttribute(getNewCollAtt());
		tb.setNewRowAttribute(getNewRowAttribList());
		tb.setNewAddedRowAttributes(getNewRowAtt());
		if(colCheckBox != null && rowCheckBox != null ) {
			tb.setSelcol(colCheckBox);
			tb.setSelrow(rowCheckBox);
		} else {
			tb.setSelcol(colCheckBox);
			tb.setSelrow(rowCheckBox);
		}*/
		String body = tb.getXHtmlTable();
		body = CheckString.replaceStringX(body, "<table>", "<table rows=\"" + tb.getTotalrows() + "\" cols=\"" + tb.getTotalcols() 
							+ "\" render=\""+prevRenderType+"\" datatype=\"" + datatype + "\" tabletype=\""
							+ tabletype + "\" primary=\""+ tb.getPrimary() + "\" style=\"" + tb.getStyle() + "\">");
		return body;
	}	
	
	private void checkTableForZeroRowZeroColumn(int totalRows, int totalCols, String tableName, String tableGridValueParam) throws Exception{
		String excMsg = "Table cannot have zero";
		if(totalRows == 0 ){
			if(tableName != null){
				excMsg = tableName+" "+excMsg+" rows";
			}else{
				excMsg = excMsg+" rows";
			}
			if(tableGridValueParam != null){
				excMsg = excMsg + "\n" + tableGridValueParam;
			}
			throw new Exception(excMsg);
		}
		if(totalCols == 0 ){
			if(tableName != null){
				excMsg = tableName+" "+excMsg+" cols";
			}else{
				excMsg = excMsg+" cols";
			}
			if(tableGridValueParam != null){
				excMsg = excMsg + "\n" + tableGridValueParam;
			}
			throw new Exception(excMsg);
		}
	}
	
	public Response<Object> getAllComponentsForCart(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getAllComponentsForCart() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();		
		List<ComponentBean> listOfcompDataFromDB=null;	
		try {
			if(!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			listOfcompDataFromDB=compDAO.getAllComponentsForCart(userName, clientId);
			
			if(!listOfcompDataFromDB.isEmpty()) {
				status.setStatusCode(200);
				status.setStatus("SUCCESS");
	    		status.setMessage("Data Found");
	    		responseVOTemp.setStatus(status);
	    		responseVOTemp.setResult(listOfcompDataFromDB);
			}else {
				status.setStatusCode(204);
				status.setStatus("No Content");
	    		status.setMessage("Data Not Found");
	    		responseVOTemp.setStatus(status);
	    		responseVOTemp.setResult(listOfcompDataFromDB);
			}    			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getAllComponentsForCart()", e);
			return response(500,"ERROR",internalServerError,listOfcompDataFromDB);
		}
		developerLog.debug("Exiting from com.dci.rest.bdo.ComponentBDO || Method Name : getAllComponentsForCart() ||");
		return responseVOTemp;
	}
	
	public Response<Object> saveAllComponentsToCart(String user, String client,ComponentBean componentBean,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : saveAllComponentsToCart() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		try {
			if(componentBean!=null && !componentBean.getfElementId().isEmpty() &&!componentBean.getDays().isEmpty()) {				
				boolean result=compDAO.saveAllComponentsToCart(user,client,componentBean.getfElementId(),componentBean.getDays());
				 if(result) {
					    status.setStatusCode(200);
						status.setStatus("SUCCESS");
			    		status.setMessage("Data Stored");
			    		responseVOTemp.setStatus(status);
				 }else {
					    status.setStatusCode(400);
						status.setStatus("Error");
			    		status.setMessage("Data Not Stored");
			    		responseVOTemp.setStatus(status);
				 }
			}else {
				status.setStatusCode(400);
				status.setStatus("Error");
	    		status.setMessage("ComponentIds & Days are Empty");
	    		responseVOTemp.setStatus(status);
			}		 
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || saveAllComponentsToCart()", e);
			return response(500,"ERROR",internalServerError,"");
		}
		developerLog.debug("Exiting from com.dci.rest.bdo.ComponentBDO || Method Name : saveAllComponentsToCart() ||");
		return responseVOTemp;
	}
	
	public Response<Object> removeComponentsFromCart(String user, String client,ComponentBean componentBean,HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : removeComponentsFromCart() ||");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();		
		try {
			if(componentBean!=null && !componentBean.getfElementId().isEmpty()) {
				 boolean result=compDAO.removeComponentsFromCart(user,client,componentBean.getfElementId());
				 if(result) {
					    status.setStatusCode(200);
						status.setStatus("SUCCESS");
			    		status.setMessage("Data Removed");
			    		responseVOTemp.setStatus(status);
				 }else {
					    status.setStatusCode(400);
						status.setStatus("Error");
			    		status.setMessage("Data Not Removed");
			    		responseVOTemp.setStatus(status);
				 }
			}else {
				status.setStatusCode(400);
				status.setStatus("Error");
	    		status.setMessage("ComponentIds is Empty");
	    		responseVOTemp.setStatus(status);
			}
			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || removeComponentsFromCart()", e);
			return response(500,"ERROR",internalServerError,"");
		}
		developerLog.debug("Exiting from com.dci.rest.bdo.ComponentBDO || Method Name : removeComponentsFromCart() ||");
		return responseVOTemp;
	}
	
	public Response<Object> getImage(String imageId, Map<String, Object> reqParams, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		ComponentBean component = new ComponentBean();
		
		String downloadPath = null;
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			if (CheckString.isNumericValue(imageId)) {

				component = compDAO.getComponent(userName, clientId, imageId);
				if(component.getBody()!=null)
				downloadPath = CheckString.getAttributeValueOfTag(component.getBody(), "graphic", "fileref").replace("file:", "");
				developerLog.debug("File path::"+downloadPath);
				if (downloadPath==null) {
					return response(404,"ERROR","","File not found");
				}
				String fileName=downloadPath.substring(downloadPath.lastIndexOf("/")+1);
				File modfile = new File(downloadPath);
				if(modfile.exists() && fileName.substring(fileName.lastIndexOf(".")+1).equalsIgnoreCase("jpg")){
					byte[] filebytes =new byte[(int)modfile.length()];
					filebytes = Files.readAllBytes(modfile.toPath());
			        component.setImageByteArray(filebytes);
					 return response(200,"SUCCESS","",component);
				}else {
					developerLog.debug("file does not exist. Try creating a new file");
					File pdffile = new File(downloadPath);
					if(pdffile.exists()){
						developerLog.debug("pdffile file exists");
						String jpgfilename = downloadPath.replaceAll(".pdf", ".jpg");
						ConvertPDFPagesToImages convertPDFPagesToImages = new ConvertPDFPagesToImages();
						boolean conversionFlag = convertPDFPagesToImages.convertPDFToImage(downloadPath, jpgfilename, "jpg");
						developerLog.debug("conversionFlag is "+conversionFlag);
						if(conversionFlag){
							developerLog.debug("New file created");
							File modfilenew = new File(jpgfilename);
							if(modfilenew.exists()){
								byte[] fileContent =new byte[(int)modfilenew.length()];
								fileContent= Files.readAllBytes(modfilenew.toPath());
						        component.setImageByteArray(fileContent);
						        return response(200,"SUCCESS","",component);
							  }
					  	}
				  }
					return response(404,"ERROR","","File not found");
			   }
			} else {
				return response(400,"ERROR",invalidCompId,imageId);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getImage()",e);
			return response(500,"ERROR",internalServerError,imageId);
		}

	}
	
	@SuppressWarnings("unused")
	private class TableInner extends DBXmlManager {
		private String getFootnotes(DocumentImpl doc, String symbol) {
			String footnotes = "";
			NodeList col = doc.getElementsByTagName(symbol);
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				if (i == 0)
					footnotes = footnotes + elem.getAttribute("value");
				else
					footnotes = footnotes + "*" + elem.getAttribute("value");
			}
			return footnotes;
		}

		private String getEntities(DocumentImpl doc, String symbol) {
			String entities = "";
			NodeList col = doc.getElementsByTagName(symbol);
			boolean first = true;
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				String type = elem.getAttribute("type");
				if (type.indexOf("entity") > -1
						&& !type.equalsIgnoreCase("sentity")) {
					if (first) {
						entities = elem.getAttribute("id");
						first = false;
					} else {
						entities = entities + "*" + elem.getAttribute("id");
					}
				}
			}
			return entities;
		}

		private String getSystemEntities(DocumentImpl doc, String symbol) {
			String entities = "";
			NodeList col = doc.getElementsByTagName(symbol);
			boolean first = true;
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				String type = elem.getAttribute("type");
				if (type.indexOf("sentity") > -1) {
					if (first) {
						entities = elem.getAttribute("id");
						first = false;
					} else {
						entities = entities + "*" + elem.getAttribute("id");
					}
				}
			}
			return entities;
		}

		private String getRecursive(DocumentImpl doc, String type) {
			String entities = "";
			NodeList col = doc.getElementsByTagName("img");
			boolean first = true;
			for (int i = 0; i < col.getLength(); i++) {
				Element elem = (Element) col.item(i);
				if (type.indexOf(elem.getAttribute("type")) > -1) {
					if (first) {
						entities = elem.getAttribute("id");
						first = false;
					} else {
						entities = entities + "*" + elem.getAttribute("id");
					}
				}
			}
			return entities;
		}

		private DocumentImpl getXmlTable(String str) throws Exception {
			StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd' []>");
			xmlString.append(StringUtility.substituteEntityName(str));
			return getDOMFromXMLString(xmlString.toString());
		}

		private String getMaxSysentitySeqId(DocumentImpl doc, String symbol) {
			int seqid =0;
			int rValue = CheckString.getInt(new TableInner().getMaxSysentitySeqId(doc,"img"));
			NodeList col = doc.getElementsByTagName(symbol);
			for (int i=0;i<col.getLength();i++) {
				Element elem = (Element)col.item(i);
				String type = elem.getAttribute("type");
				if (type.indexOf("sentity") > -1 && elem.getAttribute("sequenceid") != null)
					seqid = CheckString.getInt(elem.getAttribute("sequenceid"));
				if(rValue < seqid)	rValue = seqid;			
			}
			return ""+rValue;
		}
	}

}
