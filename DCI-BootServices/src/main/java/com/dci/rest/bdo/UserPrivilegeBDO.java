package com.dci.rest.bdo;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dci.rest.dao.auth.UserPrivilegeDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ComponentPermissionBean;
import com.dci.rest.utils.CheckString;

@Component
public class UserPrivilegeBDO {
	@Autowired UserPrivilegeDAO userPrivilege;
	private static String para ="text";
	private static String bridgehead ="subhead";
	

	public boolean checkComponentPerrmission(String user, String client, ComponentBean component) {
		boolean flag= false;
		try {
			if(CheckString.isValidString(component.getType()) && CheckString.isValidString(component.getOperation())) {
				String type = component.getType().toLowerCase();
				if(type.equalsIgnoreCase("para")) {
					type = para;
				}else if(type.equalsIgnoreCase("bridgehead")) {
					type = bridgehead;
				}
				ComponentPermissionBean privilegeBean = userPrivilege.getComponentPrivilege(user, client).get(type);
				if(privilegeBean != null) {
					if(privilegeBean.getPermission().stream().anyMatch(operation -> operation.toLowerCase().equalsIgnoreCase(component.getOperation().toLowerCase()))) {
						flag = true;
					}
				}
			}			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.UserPrivilegeBDO || checkComponentPerrmission()", e);
		}
		return flag;
	}
	
	public Map<String, ComponentPermissionBean> getComponentPrivilege(String user, String client) {
		Map<String, ComponentPermissionBean> privilegeBean= null;
		try {
			privilegeBean = userPrivilege.getComponentPrivilege(user, client);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.UserPrivilegeBDO || ComponentPermissionBean()", e);
		}
		return privilegeBean;
	}
}
